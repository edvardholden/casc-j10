import time

from context_modifiers import HEURISTIC_CONTEXT_MODIFIERS, CLAUSIFIER_CONTEXT_MODIFIERS
from run.heuristic import Heuristic, Clausifier, ClausifierMode
from run.problem import Problem
from run.process import ProverProcess, ClausifierProcess


def test_heuristic_contexts():
    # Loop through each context and check that it parses successfully with the prover

    for context in HEURISTIC_CONTEXT_MODIFIERS:
        # Acquire heuristic and process
        heuristic = Heuristic(
            "tests/res/heuristic_syntax_check.txt", context, clausifier=None
        )
        prover_process = ProverProcess(
            heuristic, Problem("tests/res/Problems/problem.p", is_ltb=False), timeout=1
        )

        # Run the prover process and make sure there are no errors
        prover_process.start()
        while prover_process.is_running():
            time.sleep(0.01)
        assert (
            not prover_process.check_errors()
        ), f'Heuristic parsing for heuristic context "{context}" failed. Verify contents'


def test_clausifier_contexts():
    for context in CLAUSIFIER_CONTEXT_MODIFIERS:
        # Acquire heuristic and process
        clausifier = Clausifier("--mode clausify -t 5", ClausifierMode.FOF, context)
        clausifier_process = ClausifierProcess(
            clausifier, Problem("tests/res/Problems/problem.p", is_ltb=False), timeout=1
        )

        # Run the prover process and make sure there are no errors
        clausifier_process.start()
        while clausifier_process.is_running():
            time.sleep(0.01)
        assert (
            not clausifier_process.check_errors()
        ), f'Heuristic parsing for clausifier context "{context}" failed. Verify contents'
