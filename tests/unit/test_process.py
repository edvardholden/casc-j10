import os
import signal
import subprocess

import pytest
import time
from mock import mock

from run.heuristic import Heuristic, Clausifier
from run.problem import Problem
from run.process import (
    is_time_left,
    get_time_left,
    ProverProcess,
    process_has_finished,
    check_prover_processes_terminated,
    start_prover_process,
    Process,
    ClausifierProcess,
    get_starexec_max_mem_cmd_str,
)


@pytest.mark.parametrize(
    "start_time, time_limit, res",
    [(time.time() - 20, 10, False), (time.time() - 10, 40, True)],
)
def test_is_time_left(start_time, time_limit, res):
    assert is_time_left(start_time, time_limit) == res


@pytest.mark.parametrize(
    "start_time, time_limit, res",
    [
        (time.time() - 20, 20, 0),
        (time.time() - 10, 40, 30),
        (time.time(), 0, 0),
        (time.time(), -5, 0),
    ],
)
def test_get_time_left(start_time, time_limit, res):
    assert pytest.approx(get_time_left(start_time, time_limit), 0.1) == res


def get_prover_process() -> ProverProcess:
    with mock.patch("run.process.utils.get_tmp_out_file") as mock_tmp:
        mock_tmp.return_value = "tmp_file"
        mock_heur = mock.Mock(spec=Heuristic)
        mock_heur.get_heuristic_cmd.return_value = "--option val"
        mock_heur.heur_file = "heur/a"
        mock_prob = mock.Mock(spec=Problem)
        mock_prob.path = "Problems/problem.p"
        return ProverProcess(mock_heur, mock_prob, 10)


def test_process_initialisation():
    p = Process(10)

    assert p.out_file is not None
    assert "_error" in p.out_file_errs
    assert p.out_err is None
    assert p.proc is None
    assert p.cmd is None
    assert p.start_time is None
    assert p.timeout == 10
    assert p.id == "ProcessBaseClass"


def test_process_get_cmd():
    p = Process(10)
    with pytest.raises(NotImplementedError):
        p._get_cmd()


def test_prover_process_initialisation():
    p = get_prover_process()
    assert p.heuristic
    assert p.timeout == 10
    assert p.problem
    assert p.id == "heur/a"
    assert "heur/a:10" in p.__str__()


@mock.patch("run.process.config.PROVER", "./iproveropt")
def test_prover_process_get_cmd():
    p = get_prover_process()
    assert (
        p._get_cmd()
        == "./iproveropt --option val Problems/problem.p 1>> tmp_file 2>> tmp_file_error"
    )
    assert mock.call.get_heuristic_cmd(p.timeout) in p.heuristic.method_calls


@mock.patch("run.process.subprocess.Popen")
@mock.patch("run.process.os.setsid", 2)
def test_prover_process_start(mock_pop):
    p = Process(10)
    mock_cmd = mock.Mock(str)
    p._get_cmd = mock_cmd
    p.start()

    mock_cmd.assert_called_once()
    assert p.start_time == pytest.approx(time.time(), 2)
    assert p.proc is mock_pop.return_value
    mock_pop.assert_called_once()


@mock.patch("run.process.utils.remove_file")
def test_prover_process_remove_out(mock_rm):
    p = get_prover_process()
    p.remove_out_files()
    assert mock_rm.call_count == 2
    assert mock.call("tmp_file") in mock_rm.call_args_list
    assert mock.call("tmp_file_error") in mock_rm.call_args_list


@mock.patch("run.process.utils.get_prover_output_status")
def test_prover_process_success_success(mock_status):
    p = get_prover_process()
    mock_status.return_value = "Theorem"
    assert p.success()


@mock.patch("run.process.utils.get_prover_output_status")
def test_prover_process_success_no_success(mock_status):
    p = get_prover_process()
    mock_status.return_value = "Stopped"
    assert not p.success()


@pytest.mark.parametrize("poll, res", [(None, True), (not None, False)])
def test_prover_process_is_running(poll, res):
    p = get_prover_process()
    p.proc = mock.Mock(poll=mock.Mock(return_value=poll))
    assert p.is_running() == res


@pytest.mark.parametrize("running, res", [(False, True), (True, False)])
def test_prover_process_is_finished(running, res):
    p = get_prover_process()
    p.is_running = mock.Mock(return_value=running)
    assert p.is_finished() == res


@mock.patch("run.process.os.killpg")
def test_prover_process_kill_not_running(mock_os):
    p = get_prover_process()
    p.is_running = mock.Mock(return_value=False)
    p.proc = mock.Mock(subprocess.Popen)
    p.kill()
    mock_os.assert_not_called()
    p.proc.communicate.assert_not_called()


@mock.patch("run.process.os.getpgid")
@mock.patch("run.process.os.killpg")
def test_prover_process_kill_is_running(mock_kill, mock_pid):
    p = get_prover_process()
    p.is_running = mock.Mock(return_value=True)
    p.proc = mock.Mock(subprocess.Popen)
    p.proc.pid = 2
    mock_pid.return_value = 5

    p.kill()
    mock_pid.assert_called_with(2)
    mock_kill.assert_called_with(5, signal.SIGKILL)
    p.proc.communicate.assert_called()


@mock.patch("run.process.os.getpgid")
def test_prover_process_kill_process_termianted(mock_pid):
    p = get_prover_process()
    p.is_running = mock.Mock(return_value=True)
    p.proc = mock.Mock(subprocess.Popen)
    p.proc.pid = 2
    mock_pid.side_effect = ProcessLookupError

    p.kill()
    mock_pid.assert_called_with(2)
    p.proc.communicate.assert_called()


def test_process_has_finished_is_finished():
    p = get_prover_process()
    p.start_time = 2
    p.is_finished = mock.Mock(return_value=True)
    assert process_has_finished(p) is True


@mock.patch("run.process.time.time", return_value=20)
@mock.patch("run.process.PROCESS_GRACE", 4)
def test_process_has_finished_overran(mock_time):
    p = mock.Mock(ProverProcess)
    p.is_finished.return_value = False
    p.start_time = 0
    p.timeout = 10

    assert process_has_finished(p) is True
    p.kill.assert_called_once()


@mock.patch("run.process.time.time", return_value=13)
@mock.patch("run.process.PROCESS_GRACE", 4)
def test_process_has_finished_not_finished(mock_time):
    p = mock.Mock(ProverProcess)
    p.is_finished.return_value = False
    p.start_time = 0
    p.timeout = 10

    assert process_has_finished(p) is False


def test_prover_process_empty_buffers_normal():
    p = get_prover_process()
    p.proc = mock.Mock(subprocess.Popen)
    p.proc.communicate = mock.Mock()

    p._empty_buffers()
    p.proc.communicate.assert_called_once_with(timeout=3)


def test_prover_process_empty_buffers_hanging():
    p = get_prover_process()
    p.proc = mock.Mock(subprocess.Popen)
    p.kill = mock.Mock()
    p.proc.communicate = mock.Mock(
        side_effect=[subprocess.TimeoutExpired("sleep 5", 3), None]
    )

    p._empty_buffers()
    assert p.proc.communicate.call_count == 2
    p.kill.assert_called_once()


@pytest.mark.parametrize("returncode, err_out", [(0, ""), (-9, "\n\n")])
def test_prover_process__check_errors_none(caplog, returncode, err_out):
    p = get_prover_process()
    p.proc = mock.Mock(subprocess.Popen)
    p.proc.returncode = returncode
    p._empty_buffers = mock.Mock()

    with mock.patch("builtins.open", mock.mock_open(read_data=err_out)):
        p.check_errors()

    p._empty_buffers.assert_called_once()
    # assert f"ran with returncode {returncode} and error: " in caplog.text
    assert len(caplog.messages) == 0
    assert caplog.text == ""


@pytest.mark.parametrize(
    "returncode, err_out",
    [(0, "error no problem"), (-9, "\nno problem\n"), (1, ""), (1, ("error"))],
)
def test_prover_process_check_errors_errors(caplog, returncode, err_out):
    p = get_prover_process()
    p.proc = mock.Mock(subprocess.Popen)
    p.proc.returncode = returncode
    p._empty_buffers = mock.Mock()

    with mock.patch("builtins.open", mock.mock_open(read_data=err_out)):
        res = p.check_errors()

    p._empty_buffers.assert_called_once()
    assert res is True
    assert f"ran with exit code {returncode} and error: " in caplog.text


@mock.patch.dict("run.process.utils.PROVER_PROCESSES", {})
def test_check_prover_processes_terminated_no_processes():
    problem = mock.Mock(Problem)
    problem.solved = False

    new_problem = check_prover_processes_terminated(problem)
    assert new_problem == problem
    assert not new_problem.solved


@mock.patch("run.process.process_has_finished", return_value=False)
def test_check_prover_processes_terminated_not_terminated(mock_finished):
    problem = mock.Mock(Problem)
    problem.solved = False

    proc = mock.Mock(ProverProcess)
    with mock.patch.dict("run.process.utils.PROVER_PROCESSES", {"1": proc}) as pdict:
        assert len(pdict) == 1
        new_problem = check_prover_processes_terminated(problem)
        assert len(pdict) == 1

    assert new_problem == problem
    assert not new_problem.solved
    mock_finished.assert_called_once_with(proc)


@mock.patch("run.process.process_has_finished", return_value=True)
def test_check_prover_processes_terminated_finished_no_success(mock_finished):
    problem = mock.Mock(Problem)
    problem.solved = False

    proc = mock.Mock(ProverProcess)
    proc.out_file = "proof.out"
    proc.szs_status = "Theorem"
    proc.success.return_value = False

    with mock.patch.dict("run.process.utils.PROVER_PROCESSES", {"1": proc}) as pdict:
        assert len(pdict) == 1
        new_problem = check_prover_processes_terminated(problem)
        assert len(pdict) == 0

    assert new_problem == problem
    assert not new_problem.solved
    mock_finished.assert_called_once_with(proc)
    proc.check_errors.assert_called_once()
    proc.success.assert_called_once()
    proc.remove_out_files.assert_called_once()


@mock.patch("run.process.process_has_finished", return_value=True)
def test_check_prover_processes_terminated_finished_success(mock_finished):
    problem = mock.Mock(Problem)
    problem.solved = False

    proc = mock.Mock(ProverProcess)
    proc.out_file = "proof.out"
    proc.szs_status = "Theorem"
    proc.success.return_value = True

    with mock.patch.dict("run.process.utils.PROVER_PROCESSES", {"1": proc}) as pdict:
        assert len(pdict) == 1
        new_problem = check_prover_processes_terminated(problem)
        assert len(pdict) == 0

    assert new_problem == problem
    mock_finished.assert_called_once_with(proc)
    proc.check_errors.assert_called_once()
    proc.success.assert_called_once()
    proc.remove_out_files.assert_not_called()
    problem.set_solved.assert_called_once_with("proof.out", "Theorem")


@mock.patch("run.process.ProverProcess")
def test_start_process_empty_dict(mock_process):
    heur = mock.Mock(Heuristic)
    prob = mock.Mock(Problem)

    proc = mock.Mock(ProverProcess)
    proc.id = "id_a"
    mock_process.return_value = proc

    with mock.patch.dict("run.process.utils.PROVER_PROCESSES", {}) as pdict:
        assert len(pdict) == 0
        start_prover_process(heur, prob, 10.0)
        assert len(pdict) == 1
        assert "id_a" in pdict

    proc.start.assert_called_once()


def get_clausifier_process() -> ClausifierProcess:
    with mock.patch("run.process.utils.get_tmp_out_file") as mock_tmp:
        mock_tmp.return_value = "tmp_file"
        mock_claus = mock.Mock(spec=Clausifier)
        mock_prob = mock.Mock(spec=Problem)
        mock_prob.path = "Problems/problem.p"
        return ClausifierProcess(mock_claus, mock_prob, 10)


def test_clausifier_process_initialisation():
    p = get_clausifier_process()
    assert isinstance(p.clausifier, Clausifier)
    assert isinstance(p.problem, Problem)
    assert p.id is not None and p.id != ""
    assert "ClausifierProcess" in str(p)


@mock.patch("run.process.config.CLAUSIFIER_PATH", "claus_path")
def test_clausifier_process_get_cmd():
    p = get_clausifier_process()
    cmd = p._get_cmd()
    p.clausifier.get_options_str.assert_called_once_with(10)
    assert "claus_path" in cmd
    assert "1>> tmp_file 2>> tmp_file_err" in cmd


@mock.patch("run.process.ProverProcess")
def test_start_process_dict_conflict(mock_process):
    heur = mock.Mock(Heuristic)
    prob = mock.Mock(Problem)

    proc = mock.Mock(ProverProcess)
    proc.id = "id_a"
    mock_process.return_value = proc

    with mock.patch.dict(
        "run.process.utils.PROVER_PROCESSES", {"id_a": mock.Mock(ProverProcess)}
    ) as pdict:
        assert len(pdict) == 1
        with pytest.raises(AssertionError):
            start_prover_process(heur, prob, 10.0)


@mock.patch.dict(os.environ, {})
def test_get_starexec_max_mem_cmd_str_no_variable():
    res = get_starexec_max_mem_cmd_str()
    assert res == ""


@mock.patch("run.process.os.cpu_count")
@mock.patch.dict(os.environ, {"STAREXEC_MAX_MEM": "128"})
def test_get_starexec_max_mem_cmd_str_single_core(mock_cpuc):
    mock_cpuc.return_value = 1
    res = get_starexec_max_mem_cmd_str()
    assert res == " ulimit -v 128000; "


@mock.patch("run.process.os.cpu_count")
@mock.patch.dict(os.environ, {"STAREXEC_MAX_MEM": "400"})
def test_get_starexec_max_mem_cmd_str_multi_core(mock_cpuc):
    mock_cpuc.return_value = 4
    res = get_starexec_max_mem_cmd_str()
    assert res == " ulimit -v 100000; "


@mock.patch("run.process.os.cpu_count")
@mock.patch.dict(os.environ, {"STAREXEC_MAX_MEM": "128"})
def test_get_starexec_max_mem_cmd_str_no_cores(mock_cpuc):
    mock_cpuc.return_value = None
    res = get_starexec_max_mem_cmd_str()
    assert res == ""
