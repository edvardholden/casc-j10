import math

import pytest
import mock

from run.heuristic import Heuristic
from run.problem import Problem
from run.schedule import (
    get_schedule_from_dict,
    flatten_schedule,
    get_schedule,
    _compute_heuristic_timeout,
    to_start_new_process,
    Schedule,
    wait_for_processes_to_terminate,
    run_schedule,
)


@mock.patch(
    "run.schedule.config_schedules.SCHEDULES", {"test": [("heur/a", 2), ("heur/b", 5)]}
)
def test_get_schedule_from_dict_single_core():
    res = get_schedule_from_dict("test")
    assert res == [("heur/a", 2), ("heur/b", 5)]


@mock.patch(
    "run.schedule.config_schedules.SCHEDULES",
    {
        "test": [[("heur/a", 2), ("heur/b", 5)]],
        "sched2": [[("heur/1", 1)], [[("heur/4", 5), ("heur/8", 4)]]],
    },
)
def test_get_schedule_from_dict_multi_core():
    res = get_schedule_from_dict("sched2")
    assert res == [[("heur/1", 1)], [[("heur/4", 5), ("heur/8", 4)]]]


@mock.patch(
    "run.schedule.config_schedules.SCHEDULES", {"test": [("heur/a", 2), ("heur/b", 5)]}
)
def test_get_schedule_from_dict_invalid_name(caplog):
    with pytest.raises(SystemExit):
        get_schedule_from_dict("does_not_exist")
    assert 'The schedule "does_not_exist" is not implemented.' in caplog.text


@pytest.mark.parametrize(
    "sched, res",
    [
        (
            [[("heur/a", 4), ("heur/b", 10), ("heur/c", 22)]],
            [("heur/a", 4), ("heur/b", 10), ("heur/c", 22)],
        ),  # Single
        (
            [[("heur/a", 10), ("heur/b", 20)], [("heur/c", 23)]],
            [("heur/a", 10), ("heur/c", 23), ("heur/b", 20)],
        ),  # Dual core
        (
            [
                [("heur/1a", 10), ("heur/1b", 10), ("heur/1c", 10)],
                [("heur/2a", 2), ("heur/2b", 4), ("heur/2c", 40), ("heur/2d", 5)],
            ],
            [
                ("heur/1a", 10),
                ("heur/2a", 2),
                ("heur/2b", 4),
                ("heur/2c", 40),
                ("heur/1b", 10),
                ("heur/1c", 10),
                ("heur/2d", 5),
            ],
        ),  # Dual - mixed
    ],
)
def test_flatten_schedule(sched, res):
    assert flatten_schedule(sched) == res


@mock.patch("run.schedule.get_heuristic")
@mock.patch("run.heuristic.HEURISTIC_CONTEXT_MODIFIERS", {"default": []})
@mock.patch(
    "run.schedule.config_schedules.SCHEDULES",
    {"test": [[("heur/a", 2), ("heur/b", 5)]]},
)
def test_get_schedule_integration(mock_heur):
    res = get_schedule("test", "default", 700)
    assert res.name == "test"
    assert res.conf == [("heur/a", 2), ("heur/b", 5)]
    assert res.no_cores == 700
    assert res.__str__() == "test:[('heur/a', 2.0), ('heur/b', 5.0)]"
    assert len(res) == 2
    mock_heur.assert_called()


@mock.patch("run.schedule.get_heuristic")
@mock.patch("run.heuristic.HEURISTIC_CONTEXT_MODIFIERS", {"default": []})
@mock.patch(
    "run.schedule.config_schedules.SCHEDULES",
    {"sched2": [[("heur/1", 1)], [("heur/4", 5), ("heur/8", None)]]},
)
def test_get_schedule_integration_flatten(mock_heur):
    res = get_schedule("sched2", "default", 4)
    assert res.conf == [("heur/1", 1), ("heur/4", 5), ("heur/8", math.inf)]
    assert res.no_cores == 4
    assert len(res) == 3
    mock_heur.assert_called()


@mock.patch(
    "run.schedule.config_schedules.SCHEDULES",
    {"test": [[("heur/a", 2), ("heur/b", 5)]]},
)
def test_get_schedule_integration_invalid():
    with pytest.raises(SystemExit):
        get_schedule("invalid", "default", 1)


@pytest.mark.parametrize(
    "args, res",
    [
        ((0, 1, 1, 10, 20.0), 20),
        ((1, 1, 1, 10, 20.0), 20),
        ((1, 1, 1, 30, 20.0), 20),
        ((1, 1, 2, 12, 20.0), 20),
        ((2, 1, 4, 12, 20.0), 20),
        ((2, 12, 4, 12, 20.0), 12),
        ((0, 20, 4, 12, 20.0), 12),
        ((9, 10, 1, 14, 20.0), 20),
        ((8, 10, 1, 14, 20.0), 14),
        ((7, 12, 4, 14, 20.0), 14),
        ((8, 12, 4, 14, 20.0), 20),
    ],
)
def test_compute_heuristic_timeout(args, res):
    assert _compute_heuristic_timeout(*args) == res


@mock.patch("run.schedule.get_heuristic")
@mock.patch(
    "run.schedule.config_schedules.SCHEDULES",
    {"test": [[("heur/a", 2), ("heur/b", 5)]]},
)
def test_schedule_get_next_heuristic(mock_heur):
    sched = get_schedule("test", "default", 1)
    _, time = sched.get_next_heuristic(10)
    assert time == 2
    _, time = sched.get_next_heuristic(4)
    assert time == 4

    with pytest.raises(IndexError):
        sched.get_next_heuristic(4)


@mock.patch("run.schedule.get_heuristic")
@mock.patch(
    "run.schedule.config_schedules.SCHEDULES",
    {"test": [[("heur/a", 2), ("heur/b", 5)]]},
)
def test_get_heuristics_left(mock_heur):
    sched = get_schedule("test", "default", 1)
    assert sched.heuristics_left()
    sched.get_next_heuristic(1)
    assert sched.heuristics_left()
    sched.get_next_heuristic(1)
    assert not sched.heuristics_left()
    with pytest.raises(IndexError):
        sched.get_next_heuristic(1)
    assert not sched.heuristics_left()


@mock.patch("run.schedule.get_heuristic")
@mock.patch("run.schedule.config_schedules.SCHEDULES", {"test": [[]]})
def test_get_heuristics_left_empty_heur(mock_heur):
    sched = get_schedule("test", "default", 1)
    assert not sched.heuristics_left()
    with pytest.raises(IndexError):
        sched.get_next_heuristic(1)
    assert not sched.heuristics_left()


@mock.patch("run.schedule.get_heuristic")
@mock.patch("run.schedule.config_schedules.SCHEDULES", {"test": [[("heur/a", 20)]]})
def test_get_heuristics_left_single_heur(mock_heur):
    sched = get_schedule("test", "default", 1)
    assert sched.heuristics_left()
    sched.get_next_heuristic(1)
    assert not sched.heuristics_left()


@mock.patch("run.schedule.get_heuristic")
@mock.patch("run.schedule.config_schedules.SCHEDULES", {"test": [[("heur/a", 20)]]})
def test_schedule_reset(mock_heur):
    sched = get_schedule("test", "default", 1)
    assert sched.heuristics_left()
    sched.get_next_heuristic(1)
    assert sched.index == 1
    sched.reset()
    assert sched.index == 0


@pytest.mark.parametrize(
    "time_left, heuristic_left, solved, exp_res",
    [
        (False, True, True, False),
        (True, False, True, False),
        (True, True, True, False),
        (True, True, False, True),
    ],
)
@mock.patch("run.schedule.is_time_left")
def test_to_start_new_process(mock_time, time_left, heuristic_left, solved, exp_res):
    mock_time.return_value = time_left

    schedule = mock.Mock(Schedule)
    schedule.heuristic_left = mock.Mock(return_value=heuristic_left)

    problem = mock.Mock(Problem)
    problem.solved = solved

    res = to_start_new_process(schedule, problem, mock.ANY, mock.ANY)
    assert res == exp_res


def get_dict_length(n):
    # Returns dict of specific length
    return {i: i for i in range(n)}


@pytest.mark.parametrize(
    "time_left, no_cores, heuristics_left, solved, pdict, exp_res",
    [
        (True, 4, True, False, get_dict_length(2), False),
        (True, 4, True, True, get_dict_length(2), False),
        (False, 4, True, False, get_dict_length(2), False),
        (True, 2, True, True, get_dict_length(2), False),
        (True, 2, True, False, get_dict_length(2), True),
        (True, 4, True, True, get_dict_length(2), False),
    ],
)
@mock.patch("run.schedule.is_time_left")
def test_wait_for_processes_to_terminate(
    mock_time, time_left, no_cores, heuristics_left, solved, pdict, exp_res
):
    mock_time.return_value = time_left

    schedule = mock.Mock(Schedule)
    schedule.no_cores = no_cores
    schedule.heuristics_left.return_value = heuristics_left

    problem = mock.Mock(Problem)
    problem.solved = solved

    with mock.patch.dict("run.schedule.PROVER_PROCESSES", pdict):
        res = wait_for_processes_to_terminate(schedule, problem, mock.ANY, mock.ANY)
    assert res == exp_res


@mock.patch("run.schedule.check_prover_processes_terminated")
@mock.patch("run.schedule.time.time")
@mock.patch("run.schedule.wait_for_processes_to_terminate")
@mock.patch("run.schedule.to_start_new_process")
def test_run_schedule_integration_mock_calls(
    mock_start_check, mock_wait, mock_time, mock_check_term
):
    mock_time.return_value = 2.0
    mock_start_check.side_effect = [True, False]
    mock_wait.side_effect = [True, False]
    mock_prob = mock.Mock(spec=Problem)
    mock_sched = mock.Mock(spec=Schedule)
    next_heur = mock.Mock(Heuristic)
    mock_sched.get_next_heuristic.return_value = (next_heur, 4.0)

    with mock.patch("run.schedule.start_prover_process") as mock_start_proc:
        _ = run_schedule(mock_prob, mock_sched, 10)

    mock_sched.get_next_heuristic.assert_called_once()
    mock_start_proc.assert_called_once_with(next_heur, mock_prob, 4.0)
    mock_check_term.assert_called_once_with(mock_prob)
