import argparse

import pytest
import mock

from run.ltb import LTBProblem
from run_ltb import (
    compute_problem_attempt_time_limit,
    clean_post_problem_attempt,
    handle_problem_post_attempt,
    terminate_batch_attempt,
    check_run_specification_args,
    get_specifications_from_args,
    LTBRunSpec,
    run_ltb_problem_attempt,
    main,
    run_ltb_batch_attempt,
)


@pytest.mark.parametrize(
    "inp, exp_res",
    [
        ((100, 2, 20), 20),
        ((10, 2, 20), 10),
        ((10, 2, -1), 5),
        ((10, 1, -1), 10),
        ((200, 4, -1), 50),
    ],
)
def test_compute_problem_attempt_time_limit(inp, exp_res):
    res = compute_problem_attempt_time_limit(
        time_left=inp[0], problems_left=inp[1], max_timelimit=inp[2]
    )
    assert res == exp_res


@mock.patch("run_ltb.utils.clean_tmp_folder_contents")
@mock.patch("run_ltb.utils.kill_all_prover_processes")
def test_clean_post_problem_attempt(mock_kill, mock_clean_folder):
    clean_post_problem_attempt()
    mock_kill.assert_called_once()
    mock_clean_folder.assert_called_once()


@mock.patch("run_ltb.handle_solved_problem")
def test_handle_problem_post_attempt_solved(mock_solved_handler, capsys):
    problem = mock.Mock()
    problem.solved = True
    problem.name = "problem.p"

    batch = mock.Mock()

    handle_problem_post_attempt(problem, batch)
    mock_solved_handler.assert_called_once_with(batch, problem)

    captured = capsys.readouterr()
    assert "% SZS status Theorem for problem.p" in captured.out
    assert "% SZS status Ended for problem.p" in captured.out


@mock.patch("run_ltb.handle_solved_problem")
def test_handle_problem_post_attempt_not_solved(mock_solved_handler, capsys):
    problem = mock.Mock()
    problem.solved = False
    problem.name = "problem.p"

    batch = mock.Mock()

    handle_problem_post_attempt(problem, batch)
    mock_solved_handler.assert_not_called()

    captured = capsys.readouterr()
    assert "% SZS status GaveUp for problem.p" in captured.out
    assert "% SZS status Ended for problem.p" in captured.out


@mock.patch("run_ltb.is_time_left")
def test_terminate_batch_attempt_no_termination(mock_time):
    mock_time.return_value = True

    mock_batch = mock.Mock()
    mock_batch.get_no_unsolved_problems.return_value = 1

    assert terminate_batch_attempt(mock_batch) is False


@mock.patch("run_ltb.is_time_left")
def test_terminate_batch_attempt_no_problems(mock_time):
    mock_time.return_value = True

    mock_batch = mock.Mock()
    mock_batch.get_no_unsolved_problems.return_value = 0

    assert terminate_batch_attempt(mock_batch) is True


@mock.patch("run_ltb.is_time_left")
def test_terminate_batch_attempt_no_time(mock_time):
    mock_time.return_value = False

    mock_batch = mock.Mock()
    mock_batch.get_no_unsolved_problems.return_value = 10

    assert terminate_batch_attempt(mock_batch) is True


@pytest.mark.parametrize(
    "schedule, version, timelimit",
    [
        [None, None, None],
        [["s"], None, None],
        [["s"], ["1"], None],
        [[], [], []],
        [["s1", "s2"], ["1"], [10]],
    ],
)
def test_check_run_specification_args_illegal(schedule, version, timelimit):
    mock_args = mock.Mock(argparse.Namespace)
    mock_args.schedule = schedule
    mock_args.version = version
    mock_args.max_timelimit = timelimit

    with pytest.raises(SystemExit):
        check_run_specification_args(mock_args)


def test_check_run_specification_args_legal():
    mock_args = mock.Mock(argparse.Namespace)
    mock_args.schedule = ["s"]
    mock_args.version = ["1"]
    mock_args.max_timelimit = [10]


@mock.patch("run_ltb.check_run_specification_args")
def test_get_specifications_from_args(mock_check):
    mock_args = mock.Mock(argparse.Namespace)
    mock_args.schedule = ["s"]
    mock_args.version = ["1"]
    mock_args.max_timelimit = [10]

    res = get_specifications_from_args(mock_args)
    assert len(res) == 1
    assert res[0].schedule == "s"
    assert res[0].version == "1"
    assert res[0].max_timelimit == 10

    mock_check.assert_called_once_with(mock_args)


def test_ltb_run_spec_init_legal():
    spec = LTBRunSpec("s", "1", 10)
    assert spec.schedule == "s"
    assert spec.version == "1"
    assert spec.max_timelimit == 10


def test_ltb_run_spec_init_legal_fair_time():
    spec = LTBRunSpec("s", "1", -1)
    assert spec.schedule == "s"
    assert spec.version == "1"
    assert spec.max_timelimit == -1


def test_ltb_run_spec_init_illegal():
    with pytest.raises(AssertionError):
        LTBRunSpec("s", "1", -20)

    with pytest.raises(AssertionError):
        LTBRunSpec("s", "1", 0)


@mock.patch("run_ltb.run_schedule")
@mock.patch("run_ltb.handle_problem_post_attempt")
@mock.patch("run_ltb.clean_post_problem_attempt")
@mock.patch("run_ltb.get_time_left")
def test_run_ltb_problem_attempt(mock_time, mock_clean, mock_handle, mock_run, capsys):
    mock_batch = mock.Mock()
    mock_prob = mock.Mock()
    mock_prob.name = "problem.p"
    mock_schedule = mock.Mock()
    mock_time.return_value = 1

    run_ltb_problem_attempt(mock_batch, mock_prob, mock_schedule, 10)

    mock_run.assert_called_once_with(mock_prob, mock_schedule, 10)
    mock_handle.assert_called_once_with(mock_prob, mock_batch)
    mock_clean.assert_called_once()

    capture = capsys.readouterr()
    assert "% SZS status Started for problem.p" in capture.out


@mock.patch("run.ltb.VERSIONS", ["1"])
@mock.patch.dict("config_schedules.SCHEDULES", {"s": [[]]})
@mock.patch(
    "sys.argv",
    [
        "program.py",
        "batch.txt",
        "out_dir",
        "--schedule",
        "s",
        "--version",
        "1",
        "--max_timelimit",
        "10",
    ],
)
@mock.patch("run_ltb.run_ltb_batch_attempt")
@mock.patch("run_ltb.terminate_batch_attempt")
@mock.patch("run_ltb.process_batch_file")
@mock.patch("run_ltb.utils")
def test_main_use_all_specifications(
    mock_utils, mock_process, mock_terminate, mock_run
):
    mock_terminate.return_value = False
    main()

    # Mostly checking the housekeeping
    mock_utils.register_functions_and_signals.assert_called_once()
    mock_process.assert_called_once()
    mock_terminate.assert_called_once()
    mock_run.assert_called_once()
    mock_utils.graceful_exit.assert_called_once()


@mock.patch("run.ltb.VERSIONS", ["1", "2"])
@mock.patch.dict("config_schedules.SCHEDULES", {"s": [[]], "s2": [[]]})
@mock.patch(
    "sys.argv",
    [
        "program.py",
        "batch.txt",
        "out_dir",
        "--schedule",
        "s",
        "s2",
        "s",
        "--version",
        "1",
        "1",
        "2",
        "--max_timelimit",
        "10",
        "20",
        "30",
    ],
)
@mock.patch("run_ltb.run_ltb_batch_attempt")
@mock.patch("run_ltb.terminate_batch_attempt")
@mock.patch("run_ltb.process_batch_file")
@mock.patch("run_ltb.utils")
def test_main_terminate(mock_utils, mock_process, mock_terminate, mock_run):
    mock_terminate.side_effect = [False, False, True]
    main()

    # Mostly checking the housekeeping
    mock_utils.register_functions_and_signals.assert_called_once()
    mock_process.assert_called_once()
    assert mock_terminate.call_count == 3
    assert mock_run.call_count == 2
    mock_utils.graceful_exit.assert_called_once()


@mock.patch.dict("run.ltb.PROBLEM_POSTFIX_TO_PROBLEM_FORMAT", {"_12": None})
@mock.patch("run.ltb.VERSIONS", ["_12"])
@mock.patch("run_ltb.run_ltb_problem_attempt")
@mock.patch("run_ltb.compute_problem_attempt_time_limit")
@mock.patch("run_ltb.get_time_left")
@mock.patch("run_ltb.terminate_batch_attempt")
@mock.patch("run_ltb.get_schedule")
def test_run_ltb_batch_attempt(
    mock_get_schedule, mock_terminate, mock_time_left, mock_comp_time, mock_attempt
):
    mock_batch = mock.Mock()
    prob1 = mock.Mock(LTBProblem)
    mock_batch.get_unsolved_problems.return_value = [
        prob1,
        mock.Mock(LTBProblem),
        mock.Mock(LTBProblem),
    ]
    mock_batch.get_no_unsolved_problems.return_value = 10
    mock_spec = mock.Mock()
    mock_spec.version = "_12"
    mock_time_left.return_value = 10
    mock_comp_time.return_value = 10

    mock_sched = mock.Mock()
    mock_get_schedule.return_value = mock_sched
    mock_terminate.side_effect = [False, False, True]

    run_ltb_batch_attempt(mock_batch, mock_spec, 6, "context")

    mock_get_schedule.assert_called_once()
    assert mock_terminate.call_count == 3
    assert mock_comp_time.call_count == 2

    prob1.set_path.assert_called_once_with(version="_12")
    assert mock_attempt.call_count == 2
    assert mock_sched.reset.call_count == 3
