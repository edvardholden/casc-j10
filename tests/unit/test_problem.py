import mock
import pytest

from run.problem import (
    Problem,
    ProblemFormat,
    infer_problem_version,
    output_proof,
    check_if_uf_logic,
)


def test_problem_version_str():
    assert str(ProblemFormat.FOF) == "fof"


@mock.patch("run.problem.os.path")
def test_problem_initialisation_exists(mock_ospath):
    mock_ospath.exists.return_value = True
    p = Problem("Problem/a.p", is_ltb=False, pformat=ProblemFormat.FOF)
    assert p.path == "Problem/a.p"
    assert p.name == "a.p"
    assert p.proof is None
    assert p.is_ltb is False
    assert p.solved is False
    assert p.szs_status == "Unknown"
    assert p.pformat is ProblemFormat.FOF
    assert p.is_tff is False

    assert "Problem:a.p" in str(p)
    assert ":fof" in str(p)


@mock.patch("run.problem.os.path")
def test_problem_initialisation_not_exists(mock_ospath):
    mock_ospath.exists.return_value = False
    with pytest.raises(ValueError):
        Problem("INVALID/Problem/a.p", False)


@mock.patch("run.problem.check_if_uf_logic")
@mock.patch("run.problem.os.path")
def test_problem_initialisation_smt_no_uf(mock_ospath, mock_uf_check):
    mock_ospath.exists.return_value = True
    mock_uf_check.return_value = False

    p = Problem("Problem/a.p", is_ltb=False, pformat=ProblemFormat.SMT2)
    assert p.pformat == ProblemFormat.SMT2
    assert p.is_ltb is True


@mock.patch("run.problem.check_if_uf_logic")
@mock.patch("run.problem.os.path")
def test_problem_initialisation_smt_uf(mock_ospath, mock_uf_check):
    mock_ospath.exists.return_value = True
    mock_uf_check.return_value = True

    p = Problem("Problem/a.p", is_ltb=False, pformat=ProblemFormat.SMT2)
    assert p.pformat == ProblemFormat.SMT2
    assert p.is_ltb is False


@mock.patch("run.problem.atexit")
@mock.patch("run.problem.os.path")
def test_problem_initialisation_set_solved(mock_ospath, mock_atexit):
    mock_ospath.exists.return_value = True

    p = Problem("Problem/a.p", is_ltb=True, pformat=ProblemFormat.FOF)
    p.set_solved("proof.out", "Theorem")
    assert p.solved
    assert p.is_ltb
    assert p.proof == "proof.out"
    assert p.szs_status == "Theorem"
    mock_atexit.register.assert_called_once()


@mock.patch("run.problem.os.path")
def test_problem_initialisation_tff_mode(mock_ospath):
    mock_ospath.exists.return_value = True

    p = Problem("Problem/a.p", is_ltb=True, pformat=ProblemFormat.TF0)
    assert p.is_tff is True


@mock.patch("run.problem.infer_problem_version")
@mock.patch("run.problem.os.path")
def test_problem_initialisation_infer_version(mock_ospath, mock_infer_version):
    mock_ospath.exists.return_value = True
    mock_infer_version.return_value = ProblemFormat.FOF

    p = Problem("Problem/a.p", False, pformat=None)
    assert p.pformat is ProblemFormat.FOF
    mock_infer_version.assert_called_once_with("Problem/a.p")


@pytest.mark.parametrize(
    "prob, pformat",
    [
        ("cnf(name, a=1)", ProblemFormat.CNF),
        ("fof(name, a=1)", ProblemFormat.FOF),
        ("tff(name, a=1)", ProblemFormat.TF0),
        ("(set-logic", ProblemFormat.SMT2),
        ("(declare-sort", ProblemFormat.SMT2),
        ("(declare-fun", ProblemFormat.SMT2),
    ],
)
def test_infer_problem_version(prob, pformat):
    with mock.patch("builtins.open", mock.mock_open(read_data=prob)):
        inferred = infer_problem_version("a.p")
    assert inferred is pformat


def test_infer_problem_version_fallback(caplog):
    with mock.patch("builtins.open", mock.mock_open(read_data="EMPTY")):
        inferred = infer_problem_version("a.p")
    assert inferred is ProblemFormat.FOF
    assert "Could not infer " in caplog.text


@pytest.mark.parametrize(
    "prob, exp_res",
    [
        ("(set-logic UF)", True),
        ("(set-logic ART)", False),
    ],
)
def test_check_if_uf_logic(prob, exp_res):
    with mock.patch("builtins.open", mock.mock_open(read_data=prob)):
        assert check_if_uf_logic("a.p") is exp_res


def test_output_proof(capsys):
    proof = """cnf(c_55,plain,
    (~state(X0,X1,X2,X3,X4,X5,X6,s2(X7,X8),X9,X10,e1(s(X7),X8),X11)|state(X0,X1,X2,X3,X4,X5,X6,s2(s(X7),X8),X9,X10,e1(X7,X8),X11)),
    file('Examples/problem.p', s2_down)).

    cnf(c_61,plain,
        (~state(X0,X1,X2,X3,X4,X5,X6,X7,X8,s4(X9,X10),e1(X9,s(X10)),X11)|
        state(X0,X1,X2,X3,X4,X5,X6,X7,X8,s4(X9,s(X10)),e1(X9,X10),X11)),
        file('Examples/problem.p', s4_right)).
    """
    with mock.patch("builtins.open", mock.mock_open(read_data=proof)):
        output_proof("a.p")

    out, err = capsys.readouterr()
    assert err == ""
    assert "cnf(c_55,plain" in out
    assert "cnf(c_61,plain" in out
