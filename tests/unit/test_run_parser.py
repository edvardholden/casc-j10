import logging

import pytest
from mock import mock

from run.problem import ProblemFormat
from run.run_parser import _get_base_parser, get_single_problem_args, get_ltb_args


def test_get_base_parser_empty_args():
    parser = _get_base_parser()
    assert parser is not None
    args = parser.parse_args(args=[])
    assert args.heuristic_context
    assert args.no_cores
    assert args.loglevel


def test_get_base_parser_args_verbose():
    parser = _get_base_parser()
    assert parser is not None
    args = parser.parse_args(
        args=["--no_cores", "4", "--heuristic_context", "default", "-v"]
    )
    assert args.heuristic_context
    assert args.no_cores == 4
    assert args.loglevel == logging.INFO


def test_get_base_parser_debug():
    parser = _get_base_parser()
    assert parser is not None
    args = parser.parse_args(args=["-d"])
    assert args.loglevel == logging.DEBUG


def test_get_single_problem_args_non_optional_arguments_no_args():
    with pytest.raises(SystemExit):
        get_single_problem_args([])


def test_get_single_problem_args_non_optional_arguments_correct_args():
    args = get_single_problem_args(["Problems/problem.p", "100"])

    assert args.problem_path == "Problems/problem.p"
    assert args.wc_timeout == 100.0
    assert args.problem_version is None
    assert args.preprocessing_heuristic is None
    assert args.preprocessing_timelimit is None


def test_get_single_problem_args_ltb_flag_false():
    args = get_single_problem_args(["Problems/problem.p", "100"])
    assert args.ltb_problem is False


def test_get_single_problem_args_set_prep_heuristic():
    args = get_single_problem_args(
        [
            "Problems/problem.p",
            "100",
            "--preprocessing_heuristic",
            "prep_heur",
            "--preprocessing_timelimit",
            "20",
        ]
    )

    assert args.preprocessing_heuristic == "prep_heur"
    assert args.preprocessing_timelimit == 20.0


def test_get_single_problem_args_ltb_flag_true():
    args = get_single_problem_args(["Problems/problem.p", "100", "--ltb_problem"])
    assert args.ltb_problem is True


def test_get_single_problem_args_set_problem_version():
    args = get_single_problem_args(
        ["Problems/problem.p", "100", "--problem_version", "fof"]
    )
    assert args.problem_version is ProblemFormat.FOF


def test_ltb_args_batch_out():
    args = get_ltb_args(args=["batch_file", "proof_out_dir"])
    assert args.batch_file == "batch_file"
    assert args.proof_out_dir == "proof_out_dir"


@pytest.mark.parametrize("inp", [[], ["batch_file"], ["out_dir"]])
def test_ltb_args_batch_illegal(inp):
    with pytest.raises(SystemExit):
        get_ltb_args(args=inp)


@mock.patch.dict("run.run_parser.config_schedules.SCHEDULES", {"t1": [[]]})
@mock.patch("run.run_parser.run.ltb.VERSIONS", ["_1"])
def test_ltb_args_single_run_conf():
    inp = [
        "batch_file",
        "out_dir",
        "--schedule",
        "t1",
        "--version",
        "_1",
        "--max_timelimit",
        "10",
    ]
    args = get_ltb_args(args=inp)
    assert args.schedule == ["t1"]
    assert args.version == ["_1"]
    assert args.max_timelimit == [10.0]


@mock.patch.dict("run.run_parser.config_schedules.SCHEDULES", {"t1": [[]], "t2": [[]]})
@mock.patch("run.run_parser.run.ltb.VERSIONS", ["_1", "_2"])
def test_ltb_args_multi_run_conf():
    inp = [
        "batch_file",
        "out_dir",
        "--schedule",
        "t1",
        "t2",
        "--version",
        "_1",
        "_2",
        "--max_timelimit",
        "10",
        "20",
    ]
    args = get_ltb_args(args=inp)
    assert args.schedule == ["t1", "t2"]
    assert args.version == ["_1", "_2"]
    assert args.max_timelimit == [10.0, 20.0]
