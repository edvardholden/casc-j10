import pytest
import mock
import logging

from run.problem import Problem
from run_problem import handle_problem_post_attempt, main


def test_handle_problem_post_attempt_unsolved(capsys):
    mock_problem = mock.Mock(solved=False, name="a.p")

    handle_problem_post_attempt(mock_problem)
    captured = capsys.readouterr()
    assert "% SZS status Unknown for" in captured.out


@mock.patch("run_problem.atexit.unregister")
@mock.patch("run_problem.output_proof")
def test_handle_problem_post_attempt_solved(mock_pout, mock_unreg, capsys):
    mock_problem = mock.Mock(
        solved=True, name="a.p", szs_status="Theorem", proof="proof.out"
    )
    handle_problem_post_attempt(mock_problem)
    captured = capsys.readouterr()
    assert "% SZS status Theorem for" in captured.out

    mock_pout.assert_called_once_with("proof.out")
    mock_unreg.assert_called_once()


def test_handle_problem_post_attempt_illegal():
    # Test the illegal combination
    mock_problem = mock.Mock(solved=True, name="a.p", proof=None)

    with pytest.raises(ValueError):
        handle_problem_post_attempt(mock_problem)


@mock.patch("run.schedule.config_schedules.SCHEDULES", {"test": [[]]})
@mock.patch(
    "sys.argv",
    [
        "main.py",
        "tests/res/Problems/problem.p",
        "300",
        "--problem_version",
        "fof",
        "--schedule",
        "test",
    ],
)
def test_main_integration_no_schedule():
    with pytest.raises(SystemExit) as exc:
        main()
    assert exc.value.code == 0


@mock.patch("run_problem.config.PROBLEM_ATTEMPT_GRACE", 12)
@mock.patch("run_problem.preprocess_problem")
@mock.patch("run_problem.utils.graceful_exit")
@mock.patch("run_problem.handle_problem_post_attempt")
@mock.patch("run_problem.utils.register_functions_and_signals")
@mock.patch("run.schedule.config_schedules.SCHEDULES", {"test": [[]]})
@mock.patch(
    "sys.argv",
    [
        "main.py",
        "tests/res/Problems/problem.p",
        "300",
        "--problem_version",
        "fof",
        "--schedule",
        "test",
        "-d",
    ],
)
def test_main_calls_no_schedule(
    mock_register, mock_post_handler, mock_graceful_exit, mock_prep_call, caplog
):
    mock_graceful_exit.side_effect = SystemExit(0)

    with pytest.raises(SystemExit) as exc:
        main()
    assert exc.value.code == 0

    # Check that logging is set to debug, as option is set
    assert logging.root.level is logging.DEBUG

    assert "new timelimit is: 312" in caplog.text

    # Check that the key functions are called
    mock_register.assert_called_once()
    mock_post_handler.assert_called_once()
    mock_prep_call.assert_not_called()
    mock_graceful_exit.assert_called_once()


@mock.patch("run_problem.config.PROBLEM_ATTEMPT_GRACE", 12)
@mock.patch("run_problem.Problem")
@mock.patch("run_problem.preprocess_problem")
@mock.patch("run_problem.utils.graceful_exit")
@mock.patch("run_problem.handle_problem_post_attempt")
@mock.patch("run_problem.utils.register_functions_and_signals")
@mock.patch("run.schedule.config_schedules.SCHEDULES", {"test": [[]]})
@mock.patch(
    "sys.argv",
    [
        "main.py",
        "tests/res/Problems/problem.p",
        "300",
        "--problem_version",
        "fof",
        "--schedule",
        "test",
        "--preprocessing_heuristic",
        "heur/heur_prep",
        "--preprocessing_timelimit",
        "20",
    ],
)
def test_main_preprocessing_call(
    mock_register,
    mock_post_handler,
    mock_graceful_exit,
    mock_prep_call,
    mock_problem_obj,
    caplog,
):
    mock_graceful_exit.side_effect = SystemExit(0)
    mock_prob = mock.Mock(Problem)
    mock_problem_obj.return_value = mock_prob

    with pytest.raises(SystemExit) as exc:
        main()
    assert exc.value.code == 0

    # Check that the key functions are called
    mock_register.assert_called_once()
    mock_post_handler.assert_called_once()
    mock_graceful_exit.assert_called_once()
    mock_prep_call.assert_called_once_with(mock_prob, "heur/heur_prep", 20.0)
