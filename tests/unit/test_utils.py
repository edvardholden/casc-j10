import logging
import signal

import mock.mock
import pytest
import mock

from run.process import ProverProcess
from run.utils import (
    check_prover_success,
    get_prover_output_status,
    get_tmp_out_file,
    remove_file,
    register_functions_and_signals,
    graceful_exit,
    kill_all_prover_processes,
    clean_tmp_dir,
    clean_tmp_folder_contents,
)


@pytest.mark.parametrize(
    "szs, res",
    [
        ("Unknown", False),
        ("Stopped", False),
        ("Theorem", True),
        ("Unsatisfiable", True),
        ("Satisfiable", True),
        ("CounterSatisfiable", True),
    ],
)
def test_check_prover_success_standard_mode(szs, res):
    assert check_prover_success(szs, ltb_mode=False) == res


@pytest.mark.parametrize(
    "szs, res",
    [
        ("Unknown", False),
        ("Stopped", False),
        ("Theorem", True),
        ("Unsatisfiable", True),
        ("Satisfiable", False),
        ("CounterSatisfiable", False),
    ],
)
def test_check_prover_success_ltb_mode(szs, res):
    assert check_prover_success(szs, ltb_mode=True) == res


@pytest.mark.parametrize(
    "pout, res",
    [
        ("", "Stopped"),
        ("% SZS sta", "Stopped"),
        ("% SZS status", "Stopped"),
        ("% SZS status Theorem", "Theorem"),
        ("before\n% SZS status Theorem", "Theorem"),
        ("% SZS status Theorem\nafter", "Theorem"),
        ("before\n% SZS status Theorem\nafter", "Theorem"),
    ],
)
def test_get_prover_output_status(pout, res):
    with mock.patch("builtins.open", mock.mock_open(read_data=pout)):
        szs = get_prover_output_status("test_file")

    assert szs == res


@mock.patch("run.utils.tempfile")
def test_get_tmp_out_file_return_file(mock_tmp):
    mock_tmp.mkstemp.return_value = (1, "tmp_dir")
    res = get_tmp_out_file()
    assert "tmp_dir" in res


@mock.patch("run.utils.os.remove")
def test_remove_file_success(mock_os):
    remove_file("test.a")
    mock_os.assert_called_once_with("test.a")


@mock.patch("run.utils.os.remove")
def test_remove_file_not_found(mock_os):
    mock_os.side_effect = FileNotFoundError
    remove_file("test.a")
    mock_os.assert_called_once_with("test.a")


@mock.patch("run.utils.signal.signal")
def test_register_functions_and_signals(mock_signal):
    register_functions_and_signals()
    mock_signal.assert_has_calls(
        [
            mock.call(signal.SIGINT, graceful_exit),
            mock.call(signal.SIGTERM, graceful_exit),
        ]
    )


@mock.patch("run.utils.logging.root.level", logging.INFO)
@mock.patch("run.utils.clean_tmp_dir")
@mock.patch("run.utils.kill_all_prover_processes")
def test_graceful_exit_no_signal_log_info(mock_kill_all, mock_clean_tmp):
    with pytest.raises(SystemExit) as exc:
        graceful_exit()
    assert exc.value.code == 0
    mock_kill_all.assert_called_once()
    mock_clean_tmp.assert_called_once()


@mock.patch("run.utils.logging.root.level", logging.DEBUG)
@mock.patch("run.utils.clean_tmp_dir")
@mock.patch("run.utils.kill_all_prover_processes")
def test_graceful_exit_no_signal_no_tmp_clean(mock_kill_all, mock_clean_tmp):
    with pytest.raises(SystemExit) as exc:
        graceful_exit()
    assert exc.value.code == 0
    mock_kill_all.assert_called_once()
    mock_clean_tmp.assert_not_called()


@mock.patch("run.utils.logging.root.level", logging.INFO)
@mock.patch("run.utils.clean_tmp_dir")
@mock.patch("run.utils.kill_all_prover_processes")
def test_graceful_exit_no_signal_tmp_clean(mock_kill_all, mock_clean_tmp, caplog):
    with pytest.raises(SystemExit) as exc:
        graceful_exit(signal_number=signal.SIGINT)
    assert exc.value.code == 0
    mock_kill_all.assert_called_once()
    mock_clean_tmp.assert_called_once()
    assert "Received signal" in caplog.text


@mock.patch.dict("run.utils.PROVER_PROCESSES", {})
def test_kill_all_processes_empty():
    # Just make sure it doesn't crash?
    kill_all_prover_processes()


def test_kill_all_processes_multiple_processes():
    proc1 = mock.Mock(ProverProcess)
    proc2 = mock.Mock(ProverProcess)
    with mock.patch.dict(
        "run.utils.PROVER_PROCESSES", {"1": proc1, "2": proc2}
    ) as proc_dict:
        assert len(proc_dict) == 2
        kill_all_prover_processes()
    proc1.kill.assert_called_once()
    proc2.kill.assert_called_once()
    assert len(proc_dict) == 0


@mock.patch("run.utils.TMP_DIR", "tmp_dir")
@mock.patch("run.utils.shutil.rmtree")
def test_clean_tmp_dir(mock_rm):
    clean_tmp_dir()
    mock_rm.assert_called_once_with("tmp_dir")


@mock.patch("run.utils.TMP_DIR", "tmp_dir")
@mock.patch("run.utils.shutil.rmtree")
def test_clean_tmp_dir_file_not_found(mock_rm):
    mock_rm.side_effect = FileNotFoundError
    clean_tmp_dir()
    mock_rm.assert_called_once_with("tmp_dir")


@mock.patch("run.utils.os")
@mock.patch("run.utils.glob")
def test_clean_tmp_folder_contents(mock_glob, mock_os):
    mock_glob.glob.return_value = ["test/file_a", "test/file_b"]
    clean_tmp_folder_contents()
    assert mock_os.remove.call_count == 2


@mock.patch("run.utils.os")
@mock.patch("run.utils.glob")
def test_clean_tmp_folder_contents_warning(mock_glob, mock_os, caplog):
    mock_glob.glob.return_value = ["test/file_a"]
    mock_os.remove.side_effect = Exception
    clean_tmp_folder_contents()
    assert mock_os.remove.call_count == 1
    assert "Could not remove" in caplog.messages[0]
