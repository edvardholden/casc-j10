import logging
import time

import mock
import pytest

from run.ltb import (
    get_batch_overall_wc,
    get_batch_problems,
    process_batch_file,
    VERSIONS,
    DEFAULT_VERSION,
    validate_problem_version_paths,
    validate_problem_version,
    LTBProblem,
    output_proof_to_file,
    LTBBatch,
    handle_solved_problem,
    PROBLEM_POSTFIX_TO_PROBLEM_FORMAT,
)
from run.problem import ProblemFormat


def test_check_version_postfix_to_format():
    # Check that all entries exist and maps to a format
    for ver in VERSIONS:
        assert ver in PROBLEM_POSTFIX_TO_PROBLEM_FORMAT
        assert isinstance(PROBLEM_POSTFIX_TO_PROBLEM_FORMAT[ver], ProblemFormat)


@pytest.mark.parametrize(
    "spec, time",
    [
        (["a"], -1),
        ([], -1),
        (["limit.time.overall.wc 50"], 50),
        (["batch start", "limit.time.overall.wc 123"], 123),
    ],
)
def test_get_batch_overall_wc(spec, time):
    assert get_batch_overall_wc(spec) == time


def test_get_batch_problems_contents():
    content = [
        "% SZS start BatchProblems",
        "Problems/a.p a",
        "Problems/b*.p b",
        "% SZS end BatchProblems",
    ]

    probs = list(get_batch_problems("bp", content))
    assert len(probs) == 2
    assert probs[0].general_path == "bp/Problems/a.p"
    assert probs[0].name == "a"
    assert probs[1].general_path == "bp/Problems/b*.p"
    assert probs[1].name == "b"


@mock.patch("run.ltb.DEFAULT_VERSION", "+1")
@mock.patch("run.ltb.VERSIONS", ["+1", "+2"])
def test_ltb_problem_initialisation():
    p = LTBProblem("problems/a*.p", "a")

    assert p.general_path == "problems/a*.p"
    assert p.name == "a"
    assert p.solved is False
    assert p.proof is None
    assert p.szs_status == "Unknown"
    assert p.path == "problems/a+1.p"
    assert p.is_ltb is True  # Always true for LTB Batch


def test_ltb_problem_set_solved():
    p = LTBProblem("problems/a*.p", "a")
    assert p.solved is False

    p.set_solved("proof.out", "Theorem")
    assert p.solved is True
    assert p.proof == "proof.out"
    assert p.szs_status == "Theorem"


@mock.patch("run.ltb.DEFAULT_VERSION", "+1")
@mock.patch("run.ltb.VERSIONS", ["+1", "+2"])
def test_ltb_problem_get_version(caplog):
    p = LTBProblem("problems/a*.p", "a")
    assert p.path == "problems/a+1.p"
    p.set_path("+2")
    assert p.path == "problems/a+2.p"
    p.set_path("+4")
    assert p.path == "problems/a+1.p"
    assert "Version +4 not recognised" in caplog.text


@pytest.mark.parametrize(
    "path, content, length",
    [
        ("path", ["% SZS start BatchProblems", "% SZS end BatchProblems"], 0),
        (
            "path",
            ["% SZS start BatchProblems", "Problems/a.p a", "% SZS end BatchProblems"],
            1,
        ),
        (
            "path",
            [
                "% SZS start BatchProblems",
                "Problems/a.p a",
                "% SZS end BatchProblems",
                "Problems/b.p b",
            ],
            1,
        ),
    ],
)
def test_get_batch_problems_lengths(path, content, length):
    assert len(get_batch_problems(path, content)) == length


@mock.patch("run.ltb.config.LTB_ATTEMPT_GRACE", 0)
@mock.patch("run.ltb.VERSIONS", ["_1"])
def test_process_batch_file():
    ltb_batch = process_batch_file("tests/res/batch_example.txt", "results/proof_out")
    assert ltb_batch.time_limit == 17280
    assert len(ltb_batch) == 4
    assert ltb_batch.output_dir == "results/proof_out"
    assert ltb_batch.get_no_unsolved_problems() == 4
    assert ltb_batch.get_no_solved_problems() == 0
    assert ltb_batch.start_time == pytest.approx(time.time())


@mock.patch("run.ltb.VERSIONS", ["_1"])
@mock.patch("run.ltb.utils")
@mock.patch("run.ltb.validate_problem_version_paths")
def test_process_batch_file_validate(mock_val, mock_utils):
    ltb_batch = process_batch_file("tests/res/batch_example.txt", "results/proof_out")
    mock_val.assert_called_once_with(ltb_batch.get_unsolved_problems())
    mock_utils.create_dir_if_not_exists.assert_called_once_with("results/proof_out")


def test_default_version_in_version():
    assert DEFAULT_VERSION in VERSIONS


def test_validate_problem_version_paths_empty():
    assert validate_problem_version_paths(set())


@mock.patch("run.ltb.VERSIONS", ["_2"])
@mock.patch("run.ltb.validate_problem_version")
@mock.patch("run.ltb.LTBProblem")
def test_validate_problem_version_paths_valid(mock_prob, mock_val):
    mock_prob.set_path.return_value = "version_2.p"
    mock_val.return_value = True
    assert validate_problem_version_paths({LTBProblem("p/a", "a")})


@mock.patch("run.ltb.VERSIONS", ["_2"])
@mock.patch("run.ltb.validate_problem_version")
@mock.patch("run.ltb.LTBProblem")
def test_validate_problem_version_paths_invalid(mock_prob, mock_val):
    mock_prob.set_path.return_value = "version_2.p"
    mock_val.return_value = False
    assert not validate_problem_version_paths({LTBProblem("p/a", "a")})


@mock.patch("run.ltb.os")
def test_validate_problem_version_exists(mock_os):
    mock_os.path.exists.return_value = True
    assert validate_problem_version("problem_path")
    mock_os.path.exists.assert_called_with("problem_path")


@mock.patch("run.ltb.os")
def test_validate_problem_version_not_exists(mock_os, caplog):
    mock_os.path.exists.return_value = False
    assert not validate_problem_version("problem_path")
    mock_os.path.exists.assert_called_with("problem_path")
    assert "Cannot find problem version: problem_path" in caplog.text
    assert caplog.record_tuples[0][1] == logging.ERROR


@mock.patch("run.ltb.shutil")
def test_output_proof_to_file(mock_shutil):
    output_proof_to_file("dest", "a.p", "proof.out")
    mock_shutil.copyfile.assert_called_once_with("proof.out", "dest/a.p")


@mock.patch("run.ltb.shutil")
def test_output_proof_to_file_trouble(mock_shutil, caplog):
    mock_shutil.copyfile.side_effect = Exception
    output_proof_to_file("dest", "a.p", "proof.out")
    mock_shutil.copyfile.assert_called_once_with("proof.out", "dest/a.p")
    assert "Could not copy proof file" in caplog.text


def get_test_problem() -> LTBProblem:
    return LTBProblem("path/a*.p", "a")


@mock.patch("run.ltb.VERSIONS", ["_1"])
def test_problem_get_path():
    assert get_test_problem().set_path("_1") == "path/a_1.p"


@mock.patch("run.ltb.VERSIONS", ["_1", "+1"])
def test_problem_str():
    prob = get_test_problem().__str__()
    assert "Name: a" in prob
    assert "Path: path/a*.p" in prob
    assert "Versions: _1, +1" in prob


@mock.patch("run.ltb.config.LTB_ATTEMPT_GRACE", 0)
def test_ltb_batch_create_with_problems():
    lb = LTBBatch("dir", 123, "out_dir", batch_problems={get_test_problem()})

    assert lb.batch_dir() == "dir"
    assert lb.output_dir == "out_dir"
    assert lb.time_limit == 123
    assert lb.get_no_solved_problems() == 0
    assert lb.get_solved_problems() == set()
    assert lb.get_no_unsolved_problems() == 1
    assert len(lb.get_unsolved_problems()) == 1


@mock.patch("run.ltb.config.LTB_ATTEMPT_GRACE", 7)
def test_ltb_batch_create_empty_problems():
    lb = LTBBatch("dir", 123, "out_dir")

    assert lb.batch_dir() == "dir"
    assert lb.output_dir == "out_dir"
    assert lb.time_limit == 130
    assert lb.get_no_solved_problems() == 0
    assert lb.get_solved_problems() == set()
    assert lb.get_no_unsolved_problems() == 0
    assert lb.get_unsolved_problems() == set()


def test_ltb_batch_create_str():
    lbs = LTBBatch("dir", 123, "out_dir", batch_problems={get_test_problem()}).__str__()
    assert "no_solved: 0" in lbs
    assert "no_unsolved: 1" in lbs


def test_ltb_batch_set_solved():
    prob = get_test_problem()
    lb = LTBBatch("dir", 123, "out_dir", {prob})
    assert lb.get_no_solved_problems() == 0
    lb.set_problem_solved(prob)
    assert len(lb) == 1

    assert lb.get_no_unsolved_problems() == 0
    assert lb.get_no_solved_problems() == 1
    assert lb.get_solved_problems() == {prob}
    assert len(lb) == 1


@mock.patch("run.ltb.output_proof_to_file")
def test_handle_solved_problem(mock_output):
    prob = get_test_problem()
    prob.proof = "proof.out"
    lb = LTBBatch("dir", 123, "out_dir", {prob})
    with mock.patch.object(lb, "set_problem_solved") as mock_batch:
        handle_solved_problem(lb, prob)

    mock_output.assert_called_once_with("out_dir", "a", "proof.out")
    mock_batch.assert_called_once_with(prob)
