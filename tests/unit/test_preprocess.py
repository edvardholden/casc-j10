import mock

from run.preprocess import preprocess_problem, handle_post_preprocess
from context_modifiers import HEURISTIC_CONTEXT_MODIFIERS
from run.problem import Problem
from run.process import ProverProcess


def test_assert_preprocessing_context():
    assert "preprocess" in HEURISTIC_CONTEXT_MODIFIERS


def test_handle_post_preprocess_success():
    mock_prob = mock.Mock(Problem)
    mock_prob.path = "problem.p"
    mock_proc = mock.Mock(ProverProcess)
    mock_proc.out_file = "out.out"
    mock_proc.szs_status = "Theorem"
    mock_proc.success.return_value = True

    handle_post_preprocess(mock_prob, mock_proc)
    mock_proc.success.assert_called_once()
    mock_prob.set_solved.assert_called_once_with("out.out", "Theorem")
    assert mock_prob.path == "problem.p"


def test_handle_post_preprocess_not_solved():
    mock_prob = mock.Mock(Problem)
    mock_prob.path = "problem.p"
    mock_proc = mock.Mock(ProverProcess)
    mock_proc.success.return_value = False
    mock_proc.out_file = "process.out"

    handle_post_preprocess(mock_prob, mock_proc)
    mock_proc.success.assert_called_once()
    mock_prob.set_solved.assert_not_called()
    assert mock_prob.path == mock_proc.out_file


@mock.patch("run.preprocess.handle_post_preprocess")
@mock.patch("run.preprocess.time")
@mock.patch("run.preprocess.process_has_finished")
@mock.patch("run.preprocess.ProverProcess")
@mock.patch("run.preprocess.get_heuristic")
def test_preprocess_problem_standard_run(
    mock_get_heur, mock_proc_obj, mock_fin, mock_time, mock_post
):
    mock_prob = mock.Mock(Problem)
    mock_prob.pformat = mock.Mock
    mock_proc = mock.Mock(ProverProcess)
    mock_proc_obj.return_value = mock_proc
    mock_proc.check_errors.return_value = False
    mock_fin.side_effect = [False, False, True]

    _ = preprocess_problem(mock_prob, "heur_file", 10)
    mock_get_heur.assert_called_once_with(
        "heur_file", heuristic_context="preprocess", pformat=mock.ANY
    )
    mock_proc.start.assert_called_once()
    assert mock_fin.call_count == 3
    assert mock_time.sleep.call_count == 3

    mock_proc.check_errors.assert_called_once()
    mock_post.assert_called_once()


@mock.patch("run.preprocess.handle_post_preprocess")
@mock.patch("run.preprocess.time")
@mock.patch("run.preprocess.process_has_finished")
@mock.patch("run.preprocess.ProverProcess")
@mock.patch("run.preprocess.get_heuristic")
def test_preprocess_problem_error(
    mock_get_heur, mock_proc_obj, mock_fin, mock_time, mock_post
):
    mock_prob = mock.Mock(Problem)
    mock_prob.pformat = mock.Mock()
    mock_proc = mock.Mock(ProverProcess)
    mock_proc_obj.return_value = mock_proc
    mock_proc.check_errors.return_value = True
    mock_fin.side_effect = [False, False, True]

    _ = preprocess_problem(mock_prob, "heur_file", 10)
    mock_get_heur.assert_called_once_with(
        "heur_file", heuristic_context="preprocess", pformat=mock.ANY
    )
    mock_proc.start.assert_called_once()

    mock_proc.check_errors.assert_called_once()
    mock_post.assert_not_called()
