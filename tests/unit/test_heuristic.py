import re

import mock
import pytest

from run.heuristic import (
    read_parameters,
    remove_unwanted_options,
    quote_list_options,
    process_clausifier_file,
    process_heuristic_file,
    ClausifierMode,
    Clausifier,
    CLAUSIFIER_PATH,
    Heuristic,
    get_heuristic,
    get_clausifier_mode,
)
from run.problem import ProblemFormat


def test_read_parameters():
    options = read_parameters("tests/res/test_heuristic_config_small.txt")
    assert len(options) == 6
    assert "clausifier" in options  # Check double dash
    assert (
        options["clausifier_options"] == "--mode clausify -t 100.00"
    )  # Check multi space


def test_read_parameters_duplicate(caplog):
    heur_file = "tests/res/test_heuristic_config_small_duplicate.txt"
    options = read_parameters(heur_file)
    assert len(options) == 4
    assert f"instantiation_flag already exists in heuristic {heur_file}" in caplog.text


@pytest.mark.parametrize(
    "options, result",
    [
        ({"inst_passive": "[]"}, {"inst_passive": '"[]"'}),
        ({"inst_passive": "[[a];[b]]"}, {"inst_passive": '"[[a];[b]]"'}),
        ({}, {}),
        ({"option": "val"}, {"option": "val"}),
    ],
)
def test_quote_list_options(options, result):
    assert quote_list_options(options) == result


@pytest.mark.parametrize(
    "options, result",
    [
        ({"time_out_real": 1}, {}),
        ({"time_out_real": 1, "clausifier": "a", "clausifier_options": "b"}, {}),
        (
            {
                "time_out_real": 1,
                "clausifier": "a",
                "clausifier_options": "b",
                "extra": -1,
            },
            {"extra": -1},
        ),
        ({"clausifier": "a", "extra": -1}, {"extra": -1}),
    ],
)
def test_remove_unwanted_options(options, result):
    assert remove_unwanted_options(options) == result


def test_process_clausifier_file():
    claus_option = process_clausifier_file("tests/res/test_heuristic_config_small.txt")
    assert claus_option == "--mode clausify -t 100.00"


def test_process_clausifier_file_no_claus_option():
    claus_option = process_clausifier_file(
        "tests/res/test_heuristic_config_small_no_clausifier_option.txt"
    )
    assert claus_option is None


def test_process_heuristic_file():
    opts = process_heuristic_file("tests/res/test_heuristic_config_small.txt")
    assert opts == {
        "instantiation_queue": '"[a]"',
        "instantiation_flag": "false",
        "preprocessed_out": "false",
    }


def test_clausifier_initialisation():
    claus = Clausifier("--mode clausify -t 100.0", ClausifierMode.FOF)
    assert claus.path == CLAUSIFIER_PATH
    assert claus.options == "--mode clausify -t 100.0"
    assert claus.clausifier_mode is ClausifierMode.FOF
    assert (
        claus.get_cmd(20)
        == f'--clausifier {CLAUSIFIER_PATH} --clausifier_options "--mode clausify -t 20.00"'
    )
    assert (
        claus.__str__()
        == "<class 'run.heuristic.Clausifier'> res/vclausify_rel --mode clausify -t 100.0"
    )


def test_clausifier_initialisation_tff():
    claus = Clausifier("--mode clausify -t 100.0", ClausifierMode.TFF)
    assert claus.path == CLAUSIFIER_PATH
    assert claus.options == "--mode tclausify -t 100.0 --show_fool true"
    assert claus.clausifier_mode is ClausifierMode.TFF
    assert (
        claus.get_cmd(20)
        == f'--clausifier {CLAUSIFIER_PATH} --clausifier_options "--mode tclausify -t 20.00 --show_fool true"'
    )


def test_clausifier_initialisation_empty_options():
    claus = Clausifier(None, ClausifierMode.FOF)
    assert claus.path == CLAUSIFIER_PATH
    assert claus.options == "--mode clausify -t 300"
    assert claus.clausifier_mode is ClausifierMode.FOF
    assert (
        claus.get_cmd(20)
        == f'--clausifier {CLAUSIFIER_PATH} --clausifier_options "--mode clausify -t 20.00"'
    )


def test_clausifier_initialisation_no_time():
    claus = Clausifier("--mode clausify", ClausifierMode.FOF)
    assert claus.options == "--mode clausify -t 10.0"


@mock.patch("run.heuristic.ClausifierMode")
def test_clausifier_invalid_mode(mock_mode):
    with pytest.raises(ValueError):
        Clausifier(None, mock_mode)


@mock.patch("run.heuristic.CLAUSIFIER_CONTEXT_MODIFIERS", {"fnt_test": ["-ss axioms"]})
def test_clausifier_set_context():
    claus = Clausifier("--mode clausify", ClausifierMode.FOF, "fnt_test")
    assert claus.options == "--mode clausify -t 10.0 -ss axioms"


@mock.patch("run.heuristic.HEURISTIC_CONTEXT_MODIFIERS", {"default": []})
def test_heuristic_initialisation_no_clausifier():
    heur = Heuristic(
        "tests/res/test_heuristic_config_small.txt",
        context="default",
        clausifier=None,
        preprocessing_flag=True,
        proof_out=True,
    )

    assert heur.get_no_options() == 6
    assert heur.get_option_value("preprocessing_flag") == "true"
    assert heur.get_option_value("proof_out") == "true"
    assert heur.context == "default"
    assert heur.clausifier is None
    assert heur.get_option_value("does_not_exist") is None
    assert "tests/res/test_heuristic_config_small.txt" in heur.__str__()
    assert "Options:6" in heur.__str__()
    assert (
        heur.get_heuristic_cmd(7)
        == '--instantiation_queue "[a]" --instantiation_flag false --preprocessed_out '
        "false --preprocessing_flag true --proof_out true --sat_out_model pos --time_out_real 7.00"
    )


@mock.patch("run.heuristic.HEURISTIC_CONTEXT_MODIFIERS", {"default": []})
def test_heuristic_initialisation_flags_false():
    heur = Heuristic(
        "tests/res/test_heuristic_config_small.txt",
        context="default",
        clausifier=None,
        preprocessing_flag=False,
        proof_out=False,
    )
    assert heur.get_no_options() == 6
    assert heur.get_option_value("preprocessing_flag") == "false"
    assert heur.get_option_value("proof_out") == "false"
    assert (
        heur.get_heuristic_cmd(7)
        == '--instantiation_queue "[a]" --instantiation_flag false --preprocessed_out '
        "false --preprocessing_flag false --proof_out false --sat_out_model none --time_out_real 7.00"
    )


@mock.patch(
    "run.heuristic.HEURISTIC_CONTEXT_MODIFIERS",
    {"smt": ["--sup_smt_interval 500", "--smt_ac_axioms fast"]},
)
def test_heuristic_initialisation_context():
    heur = Heuristic(
        "tests/res/test_heuristic_config_small.txt",
        context="smt",
        clausifier=None,
    )
    assert heur.context == "smt"
    heur_cmd = heur.get_heuristic_cmd(10)
    assert "--sup_smt_interval 500" in heur_cmd
    assert "--smt_ac_axioms fast" in heur_cmd


def test_heuristic_initialisation_clausifier():
    claus = Clausifier("--mode clausify -t 100.0", ClausifierMode.TFF)
    heur = Heuristic(
        "tests/res/test_heuristic_config_small.txt",
        clausifier=claus,
        context="default",
    )

    cmd = heur.get_heuristic_cmd(7)
    assert (
        f'--clausifier {CLAUSIFIER_PATH} --clausifier_options "--mode tclausify -t 7.00 --show_fool true"'
        in cmd
    )


def test_clausifier_smt_input_syntax():
    claus = Clausifier("--mode clausify --input_syntax fof", ClausifierMode.SMT)
    assert len(re.findall("--input_syntax", claus.options)) == 1
    assert "--input_syntax smtlib2" in claus.options


def test_get_heuristic():
    # Check that clausifier is not none
    heur = get_heuristic(
        "tests/res/test_heuristic_config_small.txt",
        "default",
        ProblemFormat.TF0,
        **{"proof_out": True},
    )
    assert heur.get_heuristic_cmd(7) != ""
    assert heur.clausifier is not None
    assert heur.clausifier.clausifier_mode is ClausifierMode.TFF


def test_get_heuristic_no_claus_options(caplog):
    # Check that clausifier is not none
    _ = get_heuristic(
        "tests/res/test_heuristic_config_no_options.txt",
        "default",
        ProblemFormat.TF0,
        **{"proof_out": True},
    )

    assert "Clausifier option not provided" in caplog.messages[0]


def test_clausifier_mode_not_implemented():
    with pytest.raises(NotImplementedError):
        get_clausifier_mode(None)  # type: ignore


def test_clausifier_mode_all_pformats():
    for pformat in ProblemFormat:
        assert isinstance(get_clausifier_mode(pformat), ClausifierMode)
