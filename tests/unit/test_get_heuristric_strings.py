import pytest
import mock
import logging
import sys

mock_db_cred = mock.Mock()
mock_db_cred.db_connection_details = {}
sys.modules["db_cred"] = mock_db_cred

from get_heuristic_strings import (  # noqa
    get_hostname,
    get_heur_string_parser,
    extract_exp_id,
    get_heuristic_names,
    hostname_heuristic_match,
    parse_heuristic,
    param_dict_to_string,
    save_heuristic,
    get_db_connection,
    get_parameter_section_from_db,
    get_heuristic_params,
    convert_binary_values,
    download_heuristic_options,
    main,
    check_heuristic_exists,
    get_heuristics,
)


def test_get_heur_string_parser_no_args():
    with pytest.raises(SystemExit):
        get_heur_string_parser(args=[])


@mock.patch.dict(
    "get_heuristic_strings.config_schedules.SCHEDULES", {"test_schedule": []}
)
def test_get_heur_string_parser_defaults():
    args = get_heur_string_parser(args=["test_schedule"])
    assert args.data[0] == "test_schedule"
    assert args.force is False
    assert args.loglevel is logging.WARNING


@mock.patch.dict(
    "get_heuristic_strings.config_schedules.SCHEDULES", {"test_schedule": []}
)
def test_get_heur_string_parser_incorrect_schedule():
    with pytest.raises(AssertionError):
        get_heur_string_parser(args=["other_schedule", "--mode", "schedule"])


@mock.patch("get_heuristic_strings.socket.gethostname", return_value="machine_name")
def test_get_hostname(mock_hostname):
    assert get_hostname() == "machine_name"


@pytest.mark.parametrize("heur_id, exp_res", [("vip_123", 123), ("321", 321)])
def test_extract_exp_id(heur_id, exp_res):
    assert extract_exp_id(heur_id) == exp_res


@mock.patch.dict(
    "get_heuristic_strings.config_schedules.SCHEDULES",
    {"test_schedule": [[("heur/a", 1), ("heur/d", 2)], [("heur/b", 7)]]},
)
def test_get_heuristic_names():
    res = get_heuristic_names("test_schedule")
    assert res == ["a", "b", "d"]


@pytest.mark.parametrize(
    "heur_id, exp_res", [("123", True), ("host_123", True), ("otherhost_123", False)]
)
def test_hostname_heuristic_match(heur_id, exp_res):
    with mock.patch("get_heuristic_strings.socket.gethostname", return_value="host"):
        assert hostname_heuristic_match(heur_id) == exp_res


def test_parse_heuristic():
    # Just want to test that it all runs
    param_dict = {
        "--include_path": "1",
        "--time_out_real": "10",
        "--other_option": "test",
    }
    assert len(parse_heuristic(param_dict)) == 1


def test_dict_to_string_empty():
    assert param_dict_to_string({}) == ""


def test_dict_to_string_multiline():
    param_dict = {"--par_b": "val_b", "--par_a": "val_a"}
    exp_res = "--par_a val_a\n--par_b val_b"
    assert param_dict_to_string(param_dict) == exp_res


@mock.patch("get_heuristic_strings.DEST_PATH", "heur")
def test_save_heuristic():
    with mock.patch("builtins.open", mock.mock_open()) as mock_open:
        save_heuristic("hostname_1234", "--param_a val_a")
        mock_open.assert_called_once_with("heur/hostname_1234", "w")


@mock.patch("get_heuristic_strings.db")
def test_get_db_connection(mock_db):
    # Very silly test
    assert len(get_db_connection()) == 2


@mock.patch("get_heuristic_strings.get_db_connection")
def test_get_parameter_section_from_db_success_query(mock_db_conn):
    mock_conn = mock.Mock()
    mock_curs = mock.Mock()
    mock_curs.fetchall.return_value = [("a", 1)]
    mock_db_conn.return_value = (mock_curs, mock_conn)

    res = get_parameter_section_from_db(1234, "parameter_section")
    assert res == [("a", 1)]

    mock_curs.execute_assert_called_once()
    mock_curs.fetchall.assert_called_once()

    exp_query = (
        "SELECT ParameterName, parameter_section FROM ExperimentProverParameters "
        "WHERE parameter_section IS NOT NULL AND Experiment=1234;"
    )
    mock_curs.execute.assert_called_once_with(exp_query)


@mock.patch("get_heuristic_strings.get_db_connection")
def test_get_parameter_section_from_db_exception_query(mock_db_conn):
    mock_conn = mock.Mock()
    mock_curs = mock.Mock()
    mock_curs.fetchall.return_value = [("a", 1)]
    mock_db_conn.return_value = (mock_curs, mock_conn)

    mock_curs.execute.side_effect = Exception

    res = get_parameter_section_from_db(1234, "parameter_section")
    assert res == []  # Failing should return empty list


def test_convert_binary_values():
    param_dict = [("--bol_a", 1), ("--bol_b", 0)]
    exp_res = [("--bol_a", "true"), ("--bol_b", "false")]
    res = convert_binary_values(param_dict)

    assert exp_res == res


def test_convert_binary_values_incorrect_input():
    param_dict = [("--bol_a", "incorrect_input"), ("--bol_b", 0)]
    with pytest.raises(ValueError):
        convert_binary_values(param_dict)


@mock.patch("get_heuristic_strings.convert_binary_values")
@mock.patch("get_heuristic_strings.get_parameter_section_from_db")
def test_get_heuristic_params(mock_db_params, mock_convert_binary):
    # Slightly pointless
    get_heuristic_params(1234)

    mock_db_params.assert_called()
    mock_convert_binary.assert_called_once()


@mock.patch("get_heuristic_strings.save_heuristic")
@mock.patch("get_heuristic_strings.hostname_heuristic_match")
def test_download_heuristic_options_no_matching_hostname(mock_match, mock_save, caplog):
    mock_match.return_value = False
    download_heuristic_options("vip_1234")

    assert "No match on hostname " in caplog.text
    mock_save.assert_not_called()


@mock.patch("get_heuristic_strings.get_heuristic_params")
@mock.patch("get_heuristic_strings.save_heuristic")
@mock.patch("get_heuristic_strings.hostname_heuristic_match")
def test_download_heuristic_options_no_params(
    mock_match, mock_save, mock_params, caplog
):
    mock_match.return_value = True
    mock_params.return_value = {}

    download_heuristic_options("vip_1234")

    assert "No parameters found " in caplog.text
    mock_save.assert_not_called()


@mock.patch("get_heuristic_strings.get_heuristic_params")
@mock.patch("get_heuristic_strings.save_heuristic")
@mock.patch("get_heuristic_strings.hostname_heuristic_match")
def test_download_heuristic_options_integration(mock_match, mock_save, mock_params):
    mock_match.return_value = True
    mock_params.return_value = {"--param_a": "val_a"}

    download_heuristic_options("vip_1234")
    mock_save.assert_called_once_with("vip_1234", "--param_a val_a")


@mock.patch.dict(
    "get_heuristic_strings.config_schedules.SCHEDULES",
    {"test_schedule": [[("heur/a", 1), ("heur/d", 2)], [("heur/b", 7)]]},
)
@mock.patch("get_heuristic_strings.download_heuristic_options")
@mock.patch("get_heuristic_strings.check_heuristic_exists")
def test_main_new_heuristics(mock_exists, mock_download):
    mock_exists.side_effect = [False, False, True]
    print(mock_exists)

    with mock.patch("get_heuristic_strings.sys.argv", ["program.py", "test_schedule"]):
        main()

    assert mock_download.call_count == 2


@mock.patch.dict(
    "get_heuristic_strings.config_schedules.SCHEDULES",
    {"test_schedule": [[("heur/a", 1), ("heur/d", 2)], [("heur/b", 7)]]},
)
@mock.patch("get_heuristic_strings.download_heuristic_options")
@mock.patch("get_heuristic_strings.check_heuristic_exists")
def test_main_force_heuristics(mock_exists, mock_download):
    mock_exists.side_effect = [False, False, True]
    print(mock_exists)

    with mock.patch(
        "get_heuristic_strings.sys.argv", ["program.py", "test_schedule", "--force"]
    ):
        main()

    assert mock_download.call_count == 3


def test_check_heuristic_exists():
    # Silly
    assert check_heuristic_exists("heur_non_existent") is False


def test_get_heuristics_invalid_mode():
    with pytest.raises(ValueError):
        get_heuristics("", [])


@mock.patch("get_heuristic_strings.get_heuristic_names")
def test_get_heuristics_schedule_mode(mock_names):
    get_heuristics("schedule", ["sched2"])
    mock_names.assert_called_once_with("sched2")


def test_get_heuristics_heuristics_mode():
    res = get_heuristics("heuristics", ["vip_222", "vip_124"])
    assert res == ["vip_222", "vip_124"]
