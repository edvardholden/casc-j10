#!/usr/bin/env python

import os.path
import socket
from pathlib import Path
from typing import List, Dict, Tuple, Any

import mysql.connector as db
import sys
import argparse
import logging

import config_schedules
import run.schedule

logging.basicConfig(
    stream=sys.stdout, level=logging.INFO, format="%(levelname)s - %(message)s"
)
log = logging.getLogger()

DEST_PATH = "heur/"


def get_heur_string_parser(args: List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "data", nargs="*", help="The list of schedules or heuristics to download"
    )

    parser.add_argument(
        "--mode",
        choices=["schedule", "heuristics"],
        default="schedule",
        help="The download mode, list of heuristics or schedule",
    )

    parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        help="Force re-downloading heuristic even if it already exists",
    )

    # Set loglevel
    parser.add_argument(
        "-d",
        "--debug",
        help="Print debug logs",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Print info logs",
        action="store_const",
        dest="loglevel",
        const=logging.INFO,
    )

    prog_args = parser.parse_args(args=args)

    if len(prog_args.data) < 1:
        raise SystemExit("Need at least one schedule/heuristic to download")

    if prog_args.mode == "schedule":
        assert len(prog_args.data) == 1, "Only downloading one schedule at a time"
        assert (
            prog_args.data[0] in config_schedules.SCHEDULES
        ), "Schedule needs to be in the schedule config"

    return prog_args


def get_db_connection():
    import db_cred  # noqa

    db_conn_details = db_cred.db_connection_details
    conn = db.connect(**db_conn_details)
    curs = conn.cursor()
    return curs, conn


def get_parameter_section_from_db(exp_id: int, section: str) -> List[Tuple[str, Any]]:
    curs, conn = None, None
    try:
        curs, conn = get_db_connection()

        query = (
            f"SELECT ParameterName, {section} "
            f"FROM ExperimentProverParameters "
            f"WHERE {section} IS NOT NULL "
            f"AND Experiment={exp_id}"
            ";"
        )

        curs.execute(query)
        res = curs.fetchall()
        return res  # type: ignore

    except Exception as err:
        log.error(err)
        return []
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def convert_binary_values(bool_params: List[Tuple[str, int]]) -> List[Tuple[str, str]]:
    # Convert boolean 0,1 to 'false','true'
    res: List[Tuple[str, str]] = []

    for param, val in bool_params:
        if val == 1:
            res.append((param, "true"))
        elif val == 0:
            res.append((param, "false"))
        else:
            raise ValueError(
                f"Boolean parameter {param} has value {val} which should be in "
                f"{{0, 1}}"
            )

    return res


def get_heuristic_params(exp_id: int) -> dict[str, str]:
    parameters = []

    # Get the parameters for the experiment
    for section in [
        "ParameterValueInt",
        "ParameterValueBool",
        "ParameterValueString",
        "ParameterValueFloat",
    ]:
        sec_params = get_parameter_section_from_db(exp_id, section)
        if section == "ParameterValueBool":
            sec_params = convert_binary_values(sec_params)
        parameters += sec_params

    param_dict = dict(parameters)
    return param_dict


def parse_heuristic(heur_params: Dict[str, str]) -> Dict[str, str]:
    # Remove some parameters as a small safeguard
    # most parameter modification happens in the Heuristic+Clausifier class

    # Remove include path
    heur_params.pop("--include_path", None)

    # Remove proof out blockers - From the old pformat
    heur_params.pop("--res_out_proof", None)
    heur_params.pop("--inst_out_proof", None)
    heur_params.pop("--sat_out_model", None)

    # Remove timeout real
    heur_params.pop("--time_out_real", None)

    # Return parameters
    return heur_params


def param_dict_to_string(heur_params: Dict[str, str]) -> str:
    # Construct heuristic string with a new option on each line
    heur_str = "\n".join(
        [param + " " + str(heur_params[param]) for param in sorted(heur_params.keys())]
    )

    # Return result
    return heur_str


def save_heuristic(heur_id: str, heur_str: str):
    with open(os.path.join(DEST_PATH, str(heur_id)), "w") as f:
        f.write(heur_str)


def get_hostname() -> str:
    return socket.gethostname().split(".")[0]


def get_heuristic_names(schedule_name: str) -> List[str]:
    # Load from the dict
    all_heur = config_schedules.SCHEDULES[schedule_name]
    # Flatten
    all_heur = run.schedule.flatten_schedule(all_heur)  # type: ignore

    # Get name only
    return sorted(set([str(Path(name).name) for name, runtime in all_heur]))  # type: ignore


def hostname_heuristic_match(heur_id: str) -> bool:
    # Syntax is "hostname_expid"
    if "_" not in heur_id:
        log.debug(f"No hostname for heuristic: {heur_id}")
        return True

    heur_hostname = heur_id.split("_")[0]
    return heur_hostname == get_hostname()


def extract_exp_id(heur_id: str) -> int:
    return int(heur_id.split("_")[-1])


def download_heuristic_options(heur_id: str) -> None:
    # Check on hostname
    if not hostname_heuristic_match(heur_id):
        log.warning(
            f"No match on hostname {get_hostname()} and heuristic host {heur_id}. Skipping..."
        )
        return

    # Extract the id
    exp_id = extract_exp_id(heur_id)

    # Get the parameters from DB
    heur_param_dict = get_heuristic_params(exp_id)
    if len(heur_param_dict) == 0:
        log.warning(f"No parameters found for experiment ID {exp_id}. Nothing to do..")
        return

    # Process the heuristic (remove timeout and other params)
    heur_param_dict = parse_heuristic(heur_param_dict)

    # Convert to string
    heur_string = param_dict_to_string(heur_param_dict)

    # Save the heuristic string
    save_heuristic(heur_id, heur_string)


def check_heuristic_exists(heur_id: str) -> bool:
    return os.path.exists(os.path.join(DEST_PATH, heur_id))


def get_heuristics(mode: str, data: List[str]) -> List[str]:
    if mode == "heuristics":
        heuristic_names = data
    elif mode == "schedule":
        heuristic_names = get_heuristic_names(data[0])
    else:
        raise ValueError(f"Mode {mode} not recognised.")

    return heuristic_names


def main():
    # Get parsing arguments
    args = get_heur_string_parser(sys.argv[1:])

    # Set the loglevel
    log.setLevel(args.loglevel)
    log.debug(str(args))

    # Extract the heuristic names from the schedule - or set from thing
    heuristic_names = get_heuristics(args.mode, args.data)

    # Download each heuristic
    for heur_id in heuristic_names:
        log.info(f"Processing heuristic: {heur_id}")

        # Check if the heuristic already exists
        if check_heuristic_exists(heur_id):
            if not args.force:
                log.info("Skipping.. Already exists")
                continue
            else:
                log.debug("Heuristic exists, but forcing re-downloading..")

        download_heuristic_options(heur_id)


if __name__ == "__main__":
    main()
