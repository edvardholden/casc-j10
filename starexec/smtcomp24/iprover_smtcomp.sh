#!/bin/bash

#export TPTP=$(./get_tptp_variable.sh $1)

#FILE_SMT=$1

# Increase the soft ulimit
ulimit -s 200000

STAREXEC_WALLCLOCK_LIMIT=1200

HERE=$(dirname "$0")

problem=$1
problem_path=$(realpath "$1")
cd "$HERE"

# Check if setting the UF schedule
grep "(set-logic UF)" "$problem_path" &> /dev/null
is_UF=$?
if [ $is_UF -eq 0 ]; 
then
    SCHEDULE="smt_comp_2024_starexec_uf"
    CONTEXT="smtcomp_uf"
else
    SCHEDULE="smt_comp_2024_starexec"
    CONTEXT="smtcomp"
fi


out=$(./run_problem "$problem_path" $STAREXEC_WALLCLOCK_LIMIT --heuristic_context $CONTEXT --no_cores 4 \
    --schedule $SCHEDULE --problem_version smt2 --suppress_proof_out )


#echo  $out |  grep "SZS"

if echo $out | grep -q "SZS status Theorem\|SZS status Unsatisfiable"
then
echo "unsat"
#exit 20
  else
  if echo $out | grep -q "SZS status CounterSatisfiable\|SZS status Satisfiable"
    then
        echo "sat"
 #       exit 10
    else
        echo "unknown"
#        exit 0
  fi
fi

