# Setup logger
from prover_driver import predict_local_schedule
import argparse
from tqdm import tqdm
from collections import Counter
import pandas as pd
import config
import sys
import logging
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format='%(levelname)s - %(message)s')
log = logging.getLogger()


PATH = ' ~/TPTP-v7.4.0/Problems/'


def run_heuristic(heur_eval, given_time):

    if heur_eval == -1 or heur_eval > given_time:
        return False, given_time
    else:
        return True, heur_eval


def attempt(evaluation, problem, schedule, wc_time):
    # Simulate the problem attempt on each core in parallel
    # Global flags of the current problem attempt
    solved = False
    solved_by = None

    # Place on single core in necesary
    if isinstance(schedule, list) and isinstance(schedule[0], tuple):
        schedule = [schedule]
        log.debug("Placing schedule onto single core")

    # Attempt the problem with the schedule of each core
    for sched in schedule:
        core_time = 0
        core_solved = False
        # Run the core schedule
        for heur, heur_time in sched:
            # Check if the heuristic can solve th eproblem with the given runtime
            core_solved, runtime = run_heuristic(
                evaluation[heur.split("/")[1]], heur_time)
            core_time += runtime

            # Check for timeout opn core
            if core_time > wc_time:
                break  # Core halted by timeout

            # Check if the problem was solved
            if core_solved:
                # If another core was faster or prev unsolved, update with the best result
                if wc_time > core_time or not solved:
                    solved = True
                    wc_time = core_time
                    solved_by = heur

                # Core solved the problem, move to the next core
                break

    return {"solved": solved, "wc_time": wc_time, "solved_by": solved_by}


def run_schedule_simulation(schedule, df, timeout, schedule_map):

    attempts = dict()
    for problem in df.index:
        if schedule_map is None:
            r = attempt(df.loc[problem], problem, schedule, timeout)
        else:
            if schedule_map[problem] == "solved":
                # Problem was solved by embedding, so just return solved
                r = {"solved": True, "wc_time": 1.0, "solved_by": "embedding"}
            else:
                r = attempt(df.loc[problem], problem,
                            schedule[schedule_map[problem]], timeout)

        attempts[problem] = r

    return attempts


def get_problem_mappings(schedule, problems):

    predictions = dict()

    log.info("Embedding problems")
    for prob in tqdm(problems):
        prob_path = PATH + prob[:3] + "/" + prob
        pred = predict_local_schedule(
            schedule, prob_path, 7, print_proof=False)  # use 7 cores
        predictions[prob] = pred

    print("## Predictions:")
    print(predictions)

    print()
    print("Predictions Overview: {0}".format(Counter(predictions.values())))

    return predictions


def remove_unsolved_problems(data):
    # List to hold which heuristics are solved
    solved_index = []

    # Store runtime data for all the problems
    all_runtimes = data.values

    #  For every problem
    for runtimes in all_runtimes:
        # Compute number of solved instances for the problem
        solved_inst = sum(runtimes > 0)
        if solved_inst > 0:
            solved_index += [1]
        else:
            solved_index += [0]

    # Add index to the df to create a filter
    data["solved_index"] = solved_index
    res = data.loc[data["solved_index"] == 1]
    print(
        "Removed {0} unsolved problems from the model data".format(
            len(data) -
            len(res)))

    return res


def main(schedule_name, data_path, timeout):

    # Get schedule from config
    try:
        schedule = config.SCHEDULES[schedule_name]
    except KeyError:
        log.error("Schedule {0} not in config".format(schedule_name))

    # Load df
    df = pd.read_csv(data_path, index_col=0)
    # Remove unsolved problems as they are unsolved anyways
    df = remove_unsolved_problems(df)

    if isinstance(schedule, dict):
        schedule_map = get_problem_mappings(schedule, df.index)
        #from mapping import predictions as schedule_map
        #print("MAP: ", Counter(schedule_map.values()))
    else:
        schedule_map = None

    attempts = run_schedule_simulation(schedule, df, timeout, schedule_map)

    solved = [prob for prob, r in attempts.items() if r["solved"]]
    print("Number of problems solved by schedule: {0}".format(len(solved)))

    heuristic_count = [prob["solved_by"]
                       for prob in attempts.values() if prob["solved_by"] is not None]
    print("Top 5 heuristics: ", Counter(heuristic_count).most_common(5))


def get_input_args(argv=None):

    parser = argparse.ArgumentParser()

    parser.add_argument('schedule_name', help='Name of the schedule (as in config)')
    parser.add_argument('data_path', help='Path to evaluation data')
    parser.add_argument('timeout', type=int, help='WC timeout for schedule simulation')

    args = parser.parse_args(argv)
    print(args)

    return args


if __name__ == "__main__":

    #schedule_name = "fof_schedule"
    #schedule_name = "sup_cicm_20_train_admissible"
    # scheudle_name =
    #data = "~/scpeduler/data/tptp_4000_super.csv"
    #timeout = 300

    # python3 simulate_schedule.py sup_cicm_20_train_admissible
    # ~/scpeduler/data/tptp_4000_super.csv 300

    #main(schedule_name, data, timeout)
    args = get_input_args()
    main(**vars(args))
