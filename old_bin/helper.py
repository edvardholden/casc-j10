import shutil
import os
import sys
import re
import atexit
import config
import tempfile
import signal
import glob
from enum import Enum, auto

# Get the root logger
import logging

log = logging.getLogger()




def compute_timeout_bound(process_timeout, wc_timeout):

    if process_timeout is None:
        timeout = wc_timeout
    else:
        timeout = min(process_timeout, wc_timeout)
    log.debug("Timeout bound: {0}".format(timeout))

    return timeout


# Make test cases first!
# Count the number of clauses in a problem (including its axioms)
def count_no_clauses(problem_path):

    # Variable to store the number of fof clauses
    count = 0

    # Set in a try as we might not find the axiom file
    # (bit dependent on the file structure)
    try:
        # First open up the problem and count the clauses
        with open(problem_path, "r") as f:
            data = f.read()

        # Get all the clauses in the problem
        count += len(re.findall("^fof", data, flags=re.MULTILINE))

        # See if there are any axiom files
        axioms = re.findall("^include.*", data, flags=re.MULTILINE)

        for axiom in axioms:
            # Get the axiom file
            ax_file = axiom[9:-3]

            # Get the path (a bit silly)
            dir_path = os.path.dirname(problem_path)
            dir_path = os.path.dirname(dir_path)
            ax_path = dir_path + "/" + ax_file

            # First open up the problem and count the clauses
            with open(ax_path, "r") as f:
                data = f.read()

            # Get all the clauses in the problem
            count += len(re.findall("^fof", data, flags=re.MULTILINE))
    except Exception:
        count = 0

    log.info("{0} clauses in {1}".format(count, problem_path))
    return count


def common_solved(u, v, **kwargs):

    # Only import if called specifically
    import numpy as np

    u = np.asarray(u, dtype=np.float32)
    v = np.asarray(v, dtype=np.float32)

    if len(u.shape) == 1:
        u = np.asarray([u], dtype=np.float32)

    if len(v.shape) == 1:
        v = np.asarray([v], dtype=np.float32)

    # Solves both
    common = np.sum(np.multiply(u, v), axis=1) * 2

    # Compute number of solved
    solved = np.sum(u, axis=1) + np.sum(v, axis=1)

    # Compute the ratio
    with np.errstate(divide="ignore", invalid="ignore"):
        ratio = np.nan_to_num(common / solved, nan=0.0)
    dist = 1 - ratio

    return dist


# Native pformat of the function above, given one dimensional vectors
def common_solved_native(u, v):

    # Compute number of problems solved in both
    common = 0
    for a, b in zip(u, v):
        common += a * b

    # Double
    common *= 2

    # Compute total number solved
    solved = sum(u) + sum(v)

    # Compute ratio
    try:
        ratio = common / solved
    except ZeroDivisionError:
        ratio = 0

    # Compute the distance
    distance = 1 - ratio
    return distance


def get_closest_cluster_center(vector, cluster_centers):

    # Compute the distance to each center
    distances = []
    for n, center in enumerate(cluster_centers):
        distance = common_solved_native(vector, center)
        distances += [distance]

    # Compute argmin
    closest_index = min(range(len(distances)), key=lambda x: distances[x])
    return closest_index
