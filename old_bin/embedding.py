
def map_embedding_to_cluster(embedding, cluster_centers):

    # Get the embedding and map to the local schedule
    try:
        cluster = hp.get_closest_cluster_center(embedding, cluster_centers)
        return cluster
    except Exception as err:
        log.warning("Unexpected exception during schedule mapping: {0}".format(err))
        # Return default scheudle if something goes wrong
        return "default"


def predict_local_schedule(schedule, problem, no_cores, print_proof=True):
    # Obtain the problem embedding
    embedding = embed_problem(schedule, problem, no_cores, print_proof=print_proof)

    # If embedding was unsuccessfull, return fallback
    if embedding is None:
        return "default"
    elif embedding == "solved":  # Used with the simulation code
        return "solved"

    cluster = map_embedding_to_cluster(embedding, schedule["cluster_centers"])
    if cluster is None:
        return "default"
    else:
        return cluster
    
    


def get_scheule_internal_prep_heuristic(schedule):

    # Get the schedule preprocessor
    try:
        internal_prep_heuristic = config.INTERNAL_PREP_HEURISTICS[schedule]
    except KeyError:
        log.warning('The prep heuristic for "{0}" is not implemented. Using default prep. '.format(schedule))
        internal_prep_heuristic = "heur/schedule_none"

    return internal_prep_heuristic


def extra():
    if isinstance(schedule_conf, dict):
        # The schedule is admissible, so find the local schedule
        log.info("Admissible schedules, predicting local schedule")
        local_schedule = predict_local_schedule(schedule_conf, problem_path, no_cores)
        log.info("Obtained local schedule: {0}".format(local_schedule))
        # Extract the appropriate schedule
        schedule_conf = schedule_conf[local_schedule]