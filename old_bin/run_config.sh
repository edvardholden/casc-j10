#!/bin/bash

#export TPTP=$(dirname $1)
#export HERE=$0
#export STAREXEC_MAX_MEM=134268928

rm outy

export STAREXEC_WALLCLOCK_LIMIT=20

./starexec_run_iprover_UEQ test_problems/COL004-3.p >> outy

./starexec_run_iprover_FNT test_problems/problem.p >> outy

./starexec_run_iprover_FOF test_problems/problem.p >> outy

