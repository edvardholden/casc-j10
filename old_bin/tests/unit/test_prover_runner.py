import os
from prover_runner import check_if_last_call, build_prover_cmd_external, build_prover_cmd_internal


def test_check_if_last_call_single_core_one_heuristics_first_heuristic():
    res = check_if_last_call(1, 1, 0)
    assert res


def test_check_if_last_call_single_core_multiple_heuristics_first_heuristic():
    res = check_if_last_call(1, 10, 0)
    assert not res


def test_check_if_last_call_single_core_multiple_heuristics_last_heuristic():
    res = check_if_last_call(1, 10, 9)
    assert res


def test_check_if_last_call_dual_cores_one_heuristic():
    res = check_if_last_call(2, 1, 0)
    assert res


def test_check_if_last_call_dual_cores_multiple_heuristics_second_heuristic():
    res = check_if_last_call(2, 5, 1)
    assert not res


def test_check_if_last_call_dual_cores_multiple_heuristics_last_heuristic():
    res = check_if_last_call(2, 5, 4)
    assert res


def test_check_if_last_call_dual_cores_multiple_heuristics_second_to_last_heuristic():
    res = check_if_last_call(2, 5, 3)
    assert res


def test_check_if_last_call_dual_cores_multiple_heuristics_third_to_last_heuristic():
    res = check_if_last_call(2, 5, 2)
    assert not res


def test_check_if_last_call_multi_cores_multiple_heuristics_first():
    res = check_if_last_call(6, 30, 0)
    assert not res


def test_check_if_last_call_multi_cores_multiple_heuristics_next_to_call():
    res = check_if_last_call(6, 30, 23)
    assert not res


def test_check_if_last_call_multi_cores_multiple_heuristics_just_in_call():
    res = check_if_last_call(6, 30, 24)
    assert res


def test_check_if_last_call_multi_cores_multiple_heuristics_in_call():
    res = check_if_last_call(6, 30, 29)
    assert res


#  "{0} {1} --time_out_real {2:.2f} {3}".pformat(config.PROVER, heur_string, timeout, problem)
#  def build_prover_cmd_external(problem, time_left, heuristic_spec, last_call=False):

def test_build_prover_cmd_external_time_last_call():
    heur_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_external("problem.p", 20, [heur_path, 10], last_call=True)

    assert timeout == 20
    assert cmd_string == "./res/iproveropt  --time_out_real 20.00 problem.p"


def test_build_prover_cmd_external_time_spec_none():
    heur_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_external("problem.p", 20, [heur_path, None], last_call=False)

    assert timeout == 20
    assert cmd_string == "./res/iproveropt  --time_out_real 20.00 problem.p"


def test_build_prover_cmd_external_time_normal_run():
    heur_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_external("problem.p", 20, [heur_path, 10], last_call=False)

    assert timeout == 10
    assert cmd_string == "./res/iproveropt  --time_out_real 10.00 problem.p"


def test_build_prover_cmd_external_time_truncate():
    heur_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_external("problem.p", 10, [heur_path, 20], last_call=False)

    assert timeout == 10
    assert cmd_string == "./res/iproveropt  --time_out_real 10.00 problem.p"


def test_build_prover_cmd_external_correct_clausifier_string():
    heur_path = os.path.dirname(__file__) + "/res/clausifier_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_external("problem.p", 100, [heur_path, 50], last_call=False)

    expt_cmd = "./res/iproveropt --clausifier res/vclausify_rel --clausifier_options \"--mode clausify\""\
        + " --schedule default --time_out_real 50.00 problem.p"

    assert timeout == 50
    assert cmd_string == expt_cmd


def test_build_prover_cmd_internal_empty_schedule():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal("problem.p", 100, [prep_path, []], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[]\" --time_out_real 0.00 problem.p"
    assert timeout == 0
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_single_schedule():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 10]]], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;10]]\" --time_out_real 11.00 problem.p"
    assert timeout == 11
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_multi_schedule():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 10], ["h2", 20], ["h3", 8], ["h4", 12]]], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;10];[h2;20];[h3;8];[h4;12]]\" --time_out_real 55.00 problem.p"
    assert int(timeout) == 55
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_single_schedule_prep_clausifier():
    prep_path = os.path.dirname(__file__) + "/res/clausifier_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 10]]], last_call=False)

    expt_cmd = "./res/iproveropt --clausifier res/vclausify_rel --clausifier_options \"--mode clausify\""\
        + " --schedule \"[[h1;10]]\" --time_out_real 11.00 problem.p"

    assert timeout == 11
    assert cmd_string == expt_cmd


def test_build_prover_cmd_internal_last_call_single_heuristic():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 10]]], last_call=True)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;-1]]\" --time_out_real 100.00 problem.p"
    assert timeout == 100
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_last_call_multi_heuristics():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 10], ["h2", 20], ["h3", 20]]], last_call=True)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;10];[h2;20];[h3;-1]]\" --time_out_real 100.00 problem.p"
    assert timeout == 100
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_none_single_heuristic():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", None]]], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;-1]]\" --time_out_real 100.00 problem.p"
    assert timeout == 100
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_none_multi_heuristics():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 10], ["h2", 20], ["h3", None]]], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;10];[h2;20];[h3;-1]]\" --time_out_real 100.00 problem.p"
    assert timeout == 100
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_no_truncate_single_heuristic():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 50]]], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;50]]\" --time_out_real 55.00 problem.p"
    assert timeout == 55
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_no_truncate_multi_heuristics():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 10], ["h2", 20], ["h3", 20]]], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;10];[h2;20];[h3;20]]\" --time_out_real 55.00 problem.p"
    assert timeout == 55
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_truncate_single_heuristic():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 200]]], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;200]]\" --time_out_real 100.00 problem.p"
    assert timeout == 100
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_truncate_multi_heuristics():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 40], ["h2", 60], ["h3", 200]]], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;40];[h2;60];[h3;200]]\" --time_out_real 100.00 problem.p"
    assert int(timeout) == 100
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_heuristic_contains_none_first():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", None], ["h2", 10]]], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;100];[h2;10]]\" --time_out_real 100.00 problem.p"
    assert timeout == 100
    assert expt_cmd == cmd_string


def test_build_prover_cmd_internal_heuristic_contains_none_middle():
    prep_path = os.path.dirname(__file__) + "/res/empty_heuristic"
    cmd_string, timeout, _ = build_prover_cmd_internal(
        "problem.p", 100, [prep_path, [["h1", 20], ["h2", None], ["h3", 30]]], last_call=False)

    expt_cmd = "./res/iproveropt  --schedule \"[[h1;20];[h2;100];[h3;30]]\" --time_out_real 100.00 problem.p"
    assert timeout == 100
    assert expt_cmd == cmd_string
