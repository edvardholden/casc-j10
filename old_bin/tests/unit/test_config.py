import os
import subprocess

from config import PROVER, PROVER_Z3, CLAUSIFIER_PATH


def helper_run_cmd(cmd):
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    try:
        outs, errs = proc.communicate(timeout=15)
        exitcode = proc.returncode
    except subprocess.TimeoutExpired:
        proc.kill()
        outs, errs = proc.communicate()
        exitcode = 127
    return outs, errs, exitcode


def test_prover_path_exists():
    assert os.path.isfile(PROVER)


def test_prover_path_run_as_expected():

    cmd = f"{PROVER} --time_out_real 10 test_problems/problem.p"
    print(cmd)
    outs, errs, exitcode = helper_run_cmd(cmd)
    assert errs == b"" or errs == b"\n\n"
    assert b"% SZS status Unsatisfiable" in outs
    assert exitcode == 0


def test_prover_z3_path_exists():
    assert os.path.isfile(PROVER_Z3)


def test_prover_z3_path_run_as_expected():

    cmd = f"{PROVER_Z3} --time_out_real 10 test_problems/problem.p"
    print(cmd)
    outs, errs, exitcode = helper_run_cmd(cmd)
    assert errs == b"" or errs == b"\n\n"
    assert b"% SZS status Unsatisfiable" in outs
    assert exitcode == 0


def test_clausifier_path_exists():
    assert os.path.isfile(CLAUSIFIER_PATH)


def test_clausifier_path_run_as_exptected():

    cmd = f"{CLAUSIFIER_PATH} --mode clausify -t 10 test_problems/problem.p"
    outs, errs, exitcode = helper_run_cmd(cmd)
    assert errs == b""
    assert exitcode == 0
    assert b"cnf(" in outs
