import os
from unittest import mock
from helper import (
    count_no_clauses,
    eprint,
    clean_tmp_folder,
    check_file_existence,
    output_proof,
    compute_timeout_bound,
    common_solved,
    common_solved_native,
    get_closest_cluster_center,
    get_problem_version_from_string,
    ProblemVersion,
    infer_problem_version,
    smt_problem_uses_uf_logic,
)
import pytest
import logging


def test_eprint(capsys):

    # Call function
    msg = "Test String"
    eprint(msg)

    # Check runtime output (Theorem)
    out, err = capsys.readouterr()
    assert out == ""
    assert err == msg + "\n"


def test_get_problem_version_from_string_cnf():
    assert get_problem_version_from_string("cnf") == ProblemVersion.CNF


def test_get_problem_version_from_string_fof():
    assert get_problem_version_from_string("fof") == ProblemVersion.FOF


def test_get_problem_version_from_string_tff():
    assert get_problem_version_from_string("tff") == ProblemVersion.TF0


def test_get_problem_version_from_string_smt():
    assert get_problem_version_from_string("smt") == ProblemVersion.SMT2


def test_get_problem_version_from_string_tx0():
    assert get_problem_version_from_string("tx0") == ProblemVersion.TX0


def test_get_problem_version_from_string_error():
    with pytest.raises(ValueError):
        assert get_problem_version_from_string("undefined")


# @mock.patch('builtins.open')
# def test_count_no_clauses_exception(mock_open):
#    mock_open.side_effects = FileNotFoundError


@mock.patch(
    "helper.read_problem", return_value="cnf(absorption,axiom,( meet(X,join(X,Y)) = X )).", auto_spec=True
)
def test_infer_problem_version_cnf(mock_problem):
    assert infer_problem_version("problem_path") == ProblemVersion.CNF


@mock.patch("helper.read_problem", return_value="fof(pel55_2_1,axiom, ( lives(agatha) )).", auto_spec=True)
def test_infer_problem_version_fof(mock_problem, caplog):
    with caplog.at_level(logging.WARNING):
        assert infer_problem_version("problem_path") == ProblemVersion.FOF
    assert caplog.text == ""


@mock.patch("helper.read_problem", return_value="tff(ty_n_tf__c, type, c : $tType).", auto_spec=True)
def test_infer_problem_version_tff(mock_problem):
    assert infer_problem_version("problem_path") == ProblemVersion.TF0


@mock.patch("helper.read_problem", return_value="(set-logic AUFLIA)", auto_spec=True)
def test_infer_problem_version_smt_logic(mock_problem):
    assert infer_problem_version("problem_path") == ProblemVersion.SMT2


@mock.patch("helper.read_problem", return_value="(declare-sort C_set$ 0)", auto_spec=True)
def test_infer_problem_version_smt_sort(mock_problem):
    assert infer_problem_version("problem_path") == ProblemVersion.SMT2


@mock.patch(
    "helper.read_problem", return_value="(declare-fun bot$ () A_character_data_ptr_set$)", auto_spec=True
)
def test_infer_problem_version_smt_fun(mock_problem):
    assert infer_problem_version("problem_path") == ProblemVersion.SMT2


@mock.patch("helper.read_problem", return_value="fallback_case", auto_spec=True)
def test_infer_problem_version_error(mock_problem, caplog):
    with caplog.at_level(logging.WARNING):
        assert infer_problem_version("problem_path") == ProblemVersion.FOF
    assert "Could not infer the problem pformat. Setting FOF as default" in caplog.text


@mock.patch("helper.read_problem", return_value="(set-logic UF)", auto_spec=True)
def test_smt_problem_uses_uf_logic_true(mock_problem):
    assert smt_problem_uses_uf_logic("problem_path")


@mock.patch("helper.read_problem", return_value="(set-logic AUFLIA)", auto_spec=True)
def test_smt_problem_uses_uf_logic_false(mock_problem):
    assert not smt_problem_uses_uf_logic("problem_path")


""" TODO does this still exists?
@mock.patch('os.mkdir')
def test_create_tmp_dir_sucess(mock_mkdir):
    create_tmp_dir()
    mock_mkdir.assert_called_once_with("/tmp/iprover_out")


@mock.patch('os.mkdir')
def test_create_tmp_dir_ignore(mock_mkdir):
    mock_mkdir.side_effect = FileExistsError
    create_tmp_dir()
    mock_mkdir.assert_called_once_with("/tmp/iprover_out")
"""


"""TODO
@mock.patch('shutil.rmtree')
def test_clean_tmp_folder_success(mock_rmtree):
    clean_tmp_folder()
    mock_rmtree.assert_called_once_with("/tmp/iprover_out")


@mock.patch('shutil.rmtree')
def test_clean_tmp_folder_ignore(mock_rmtree):
    mock_rmtree.side_effect = FileNotFoundError
    clean_tmp_folder()
    mock_rmtree.assert_called_once_with("/tmp/iprover_out")
"""


@mock.patch("os.path.exists")
def test_check_file_existence_success(mock_exists):
    mock_exists.return_value = True
    check_file_existence("test_file")
    mock_exists.assert_called_once_with("test_file")


@mock.patch("os.path.exists")
def test_check_file_existence_fail(mock_exists, capsys):
    mock_exists.return_value = False

    with pytest.raises(SystemExit) as catch_exit:
        check_file_existence("test_file")
    assert catch_exit.value.code == 1

    mock_exists.assert_called_once_with("test_file")
    out, err = capsys.readouterr()
    assert out == ""
    assert err == 'ERROR: file "test_file" does not exist\n'


""" TODO
def test_output_proof(capsys):
    proof_file = os.path.dirname(__file__) + "/res/dummy_proof.txt"
    output_proof(proof_file)
    out, err = capsys.readouterr()
    assert out == "1+1=2\n\n"
"""


def test_compute_timeout_bound_no_proc():
    res = compute_timeout_bound(None, 5)
    assert res == 5


def test_compute_timeout_bound_wc_higher():
    res = compute_timeout_bound(3, 5)
    assert res == 3


def test_compute_timeout_bound_wc_lower():
    res = compute_timeout_bound(20, 4)
    assert res == 4


def test_count_no_clauses_no_axiom_file():
    problem_path = os.path.dirname(__file__) + "/res/problem_fof.p"
    res = count_no_clauses(problem_path)

    assert res == 1


def test_count_no_clauses_single_axiom_file():
    problem_path = os.path.dirname(__file__) + "/../../../ltb_test_res/Problems/HL400001+4.p"
    res = count_no_clauses(problem_path)

    assert res == 45


def test_count_no_clauses_multiple_axiom_files():
    problem_path = os.path.dirname(__file__) + "/../../../ltb_test_res/Problems/HL400001+5.p"
    res = count_no_clauses(problem_path)

    assert res == 72


@mock.patch("builtins.open")
def test_count_no_clauses_exception(mock_open):
    mock_open.side_effects = FileNotFoundError
    res = count_no_clauses("dummy_path")

    assert res == 0


def test_common_solved_all_zeroes():
    res = common_solved([0, 0, 0], [0, 0, 0])
    assert res == 1


def test_common_solved_all_ones():
    res = common_solved([1, 1, 1], [1, 1, 1])
    assert res == 0


def test_common_solved_complement():
    res = common_solved([0, 1, 0], [1, 0, 1])
    assert res == 1


def test_common_solved_1():
    res = common_solved([1, 1, 1], [1, 0, 0])
    assert res == 0.5


def test_common_solved_2():
    res = common_solved([1, 1, 0, 0], [1, 0, 1, 0])
    assert res == 0.5


def test_common_solved_3():
    res = common_solved([1, 0, 0, 0], [1, 1, 1, 1])
    assert res == 0.6


def test_common_solved_4():
    res = common_solved_native([1, 1, 1, 1], [0, 0, 0, 0])
    assert res == 1


def test_common_solved_equal():
    res = common_solved([1, 1, 0], [1, 1, 0])
    assert res == 0


def test_common_solved_native_all_zeroes():
    res = common_solved_native([0, 0, 0], [0, 0, 0])
    assert res == 1


def test_common_solved_native_all_ones():
    res = common_solved_native([1, 1, 1], [1, 1, 1])
    assert res == 0


def test_common_solved_native_complement():
    res = common_solved_native([0, 1, 0], [1, 0, 1])
    assert res == 1


def test_common_solved_native_1():
    res = common_solved_native([1, 1, 1], [1, 0, 0])
    assert res == 0.5


def test_common_solved_native_2():
    res = common_solved_native([1, 1, 0, 0], [1, 0, 1, 0])
    assert res == 0.5


def test_common_solved_native_3():
    res = common_solved_native([1, 0, 0, 0], [1, 1, 1, 1])
    assert res == 0.6


def test_common_solved_native_4():
    res = common_solved_native([1, 1, 1, 1], [0, 0, 0, 0])
    assert res == 1


def test_common_solved_native_equal():
    res = common_solved_native([1, 1, 0], [1, 1, 0])
    assert res == 0


def test_get_closest_cluster_center_single():
    vector = [1, 1, 1, 1]
    cluster_centers = [[1, 1, 1, 1]]
    res = get_closest_cluster_center(vector, cluster_centers)

    assert res == 0


def test_get_closest_cluster_center_first():
    vector = [1, 1, 1, 1]
    cluster_centers = [[1, 1, 1, 1], [1, 1, 0, 0], [0, 0, 0, 0]]
    res = get_closest_cluster_center(vector, cluster_centers)

    assert res == 0


def test_get_closest_cluster_center_middle():
    vector = [1, 1, 1, 1]
    cluster_centers = [[0, 0, 0, 1], [1, 0, 1, 1], [1, 1, 0, 0]]
    res = get_closest_cluster_center(vector, cluster_centers)

    assert res == 1


def test_get_closest_cluster_center_last():
    vector = [1, 0, 1, 0]
    cluster_centers = [[0, 0, 0, 1], [0, 1, 0, 0], [1, 1, 0, 0]]
    res = get_closest_cluster_center(vector, cluster_centers)

    assert res == 2
