# docker build -t smtcomp-iprover:leader .
#FROM satcomp-infrastructure:common


FROM satcomp-infrastructure:leader
WORKDIR /


# Install python version 3.10
USER root
RUN apt-get update && \
    apt install -y curl wget build-essential libncursesw5-dev libssl-dev \
libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev  python-html5lib && \
    apt install -y software-properties-common && \
    add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get update && \
    apt install -y python3.10 && \
    apt install -y python3.10-distutils
# Get latest version of pip
#RUN curl -sS https://bootstrap.pypa.io/get-pip.py | python3.10

# Isntall freeze package
#RUN python3.10 -m pip install cx_Freeze

# Copy over the main files
COPY heur /competition/heur
COPY src/run /competition/run
COPY config.py /competition
COPY config_schedules.py /competition
COPY run_problem.py /competition

# Pick the resources
#COPY res /competition/res
COPY res/iproveropt_static_z3 /competition/res/
COPY res/vclausify_rel /competition/res/

# Compile the run scripts
#rrun_problem.py "$1" 1200 --RUN cd /competition && python3.10 -m cx_Freeze -c run_problem.py --target-dir dist -s
#RUN cp -r /competition/dist/* /competition
## Set the run script as an executable
#RUN chmod +x /competition/run_problem

# Add AWS specific scripts and set as executable
COPY --chown=ecs-user aws/init_solver.sh /competition
COPY --chown=ecs-user aws/run_solver.sh /competition
COPY --chown=ecs-user aws/solver /competition
USER ecs-user
RUN chmod +x /competition/init_solver.sh
RUN chmod +x /competition/run_solver.sh
RUN chmod +x /competition/solver

EXPOSE 22