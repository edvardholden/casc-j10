

# ICJAR-J10 iProver
[![pipeline status](https://gitlab.com/edvardholden/casc-j10/badges/master/pipeline.svg)](https://gitlab.com/edvardholden/casc-j10/-/commits/master)
[![coverage report](https://gitlab.com/edvardholden/casc-j10/badges/master/coverage.svg)](https://gitlab.com/edvardholden/casc-j10/-/commits/master)

This project contains the setup, heuristics and configurations of iProver in CASC-J10 and the later editions of CASC.


## Installation

This project expects python3 (3.11 >=)

Clone this repository by running:
```bash
git clone git@gitlab.com:edvardholden/casc-j10.git
pip3 install -r requirements.txt
pip3 install -e .
```

# Quick Start


### General Usage - Single Problem

To run the prover driver on a problem with a timeout run:
```bash
python3 -u run_problem.py <problem_path> <wallclock>
```
See `--help` for more information about various run configurations and schedules.


## LTB Mode

To run the prover driver for LTB batch, give the path to the batch specification file and the output directory.
The batch attempts are specified through lists of ltb attempt specifiers consisting of (schedule, version, max_timelimit).

```bash
python3 -u run_ltb.py <batch_specification> <output_dir> \
--schedule <schedule_list> --version <version_list> --max_timelimit <time_limits_list>
```


## Deployment

### Deployment On StarExec


The scripts can easily be deployed to StarExec by running:
```bash
./deploy_starexec.sh
```
This will result in a `bin.zip` file can be uploaded to StarExec as the prover.
This file will consist of the `starexec_run_*` scripts specified in the deployment directory (see script),
the python driver scripts, the src folder, a prover description and installation script.


### Deployment On AWS

The repo contains a `Dockerfile` which can be used to create a docker mage for deployment onto AWS.
In the current state, this image uses the result from cx_freeze.
Hence, `deploy_starexec.sh` should be run before creating the image.

For information and scripts for local/remote testing of the scripts, look into the `aws/` dir.


# Developer  Guide

## Running Tests

To install the test libraries and the package:

```bash
pip3 install -r requirements_dev.txt
pip3 install -e .
```

The test suite is run with pytest:
```bash
python3 -m pytest
```

The full quality check is run with tox:
```bash
tox
```

## Updating iProver


iProver can be installed from [gitlab](https://gitlab.com/korovin/iprover).

To compile iProver statically to use on StarExec compile with flag `STATIC=true`.
```bash
./configure && make STATIC=true
```
Once you have compiled iprover, copy the iproveropt executable to 'res' as the prover path is set as `res/iproveropt`.
If compiling with Z3, add it to the appropriate path set in `config.py`


## Schedules

### Adding Schedules

Schedules are specified in `config_schedules.SCHEDULES` and they have the form `List[List[Tuple[str, int|float|None]]]`.
None represents unbounded (max) runtime, while the numerical values specifies a set runtime that might be truncated or extended depending on the time available.

The heuristic specification files are found in 'heur/' as specified by the `DEST_PATH` variable.

NEVER under any circumstance edit the heuristic files, as this may cause major issues and confusion.
If a set of heuristics should be amended due to a specific scenario, this can be specified through
the context dictionaries in `context_modifiers.py`.
Next, the context can be provided for the given run scenario.

### Downloading Heuristics

To download the heuristics of a schedule run
```bash
python3 get_heuristic_strings.py <schedule_name>
```
This requires the `db_cred.py` files with the correct credentials, and downloads the prover parameters from the database
and places them in a corresponding file.
New versions of the heuristics should have the hostname of the database/cluster in the name.

### Testing Schedules and Heuristics

To verify that the heuristics and schedules are specified correctly, run
```bash
python3 validate_schedules.py
```

# TODO - Live tests

To test the configuration scripts for the non-LTB divisions run `./run_config`.
The test passes if there is no output.

To test the LTB configuration run `./run_config_ltb`.
This should result in the expected competition output for the batch file.


## Authors

* **Edvard Holden** 
* **Konstantin Korovin**


## Acknowledgments

* Efficient and fast heuristics were discovered with [iProver-SMAC](https://gitlab.com/edvardholden/scpeduler) 
* The schedules were built automatically using [SCPeduler](https://gitlab.com/korovin/iprover-smac)

