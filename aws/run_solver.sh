#!/bin/bash


## Truncate the cores if needed
#no_cores="$(( nproc < 15 ? nproc : 15 ))"
max=15
s_core=$(nproc)
n_cores="$(( s_core < max ? s_core : max ))"
#echo "$n_cores"

cd /competition;

grep "(set-logic UF)" $1 &> /dev/null
is_UF=$?
if [ $is_UF -eq 0 ];
then
    SCHEDULE="smt_comp_2023_aws_uf"
else
    SCHEDULE="smt_comp_2023_aws"
fi





#./run_problem "$1" 1200 --heuristic_context smtcomp --no_cores "$n_cores" \
#--schedule $SCHEDULE --problem_version smt2 --suppress_proof_out


python3.10 -u run_problem.py "$1" 1200 --heuristic_context smtcomp --no_cores "$n_cores" \
--schedule $SCHEDULE --problem_version smt2 --suppress_proof_out
