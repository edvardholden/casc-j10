import logging
import sys
from typing import Optional
import boto3
from botocore.exceptions import ProfileNotFound

logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
logger = logging.getLogger("Quickstart Run")
logger.setLevel(logging.INFO)


class STS:
    def __init__(self, sts_client) -> None:
        self.sts = sts_client

    def get_account_number(self) -> str:
        try:
            return self.sts.get_caller_identity()["Account"]
        except Exception as e:
            logger.error(f"Failed to get profile account number: {e}")
            raise e


def get_sts_session(profile: Optional[str]):
    try:
        if profile:
            session = boto3.Session(profile_name=profile)
        else:
            session = boto3.Session()
    except ProfileNotFound as e:
        logger.error(
            f"Unable to create AWS session.  Please check that default profile is set up in the ~/.aws/config file and has appropriate rights (or if --profile was provided, that this profile has appropriate rights). Error: {e}"
        )
        sys.exit(1)

    sts = STS(session.client("sts"))
    return sts
