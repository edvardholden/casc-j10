
# AWS Deployment Scripts for SMTCOMP - Parallel (AWS)

This directory provides information and scripts for how to run and test the iProver Docker
image for the SMTCOMP infrastructure.

These instructions are a summary and adaptation of the original repo found [here](https://github.com/aws-samples/aws-batch-comp-infrastructure-sample/tree/mainline).
The requirements are likely to change for each iteration of the competition.


## Requirements:

To use the AWS infrastructure, you will need the following tools installed:

- python3
- [boto3](https://aws.amazon.com/sdk-for-python/).  you can install this with `pip3 install boto3`. 
- [docker](https://www.docker.com/).  There is a button to download Docker Desktop on the main page.
- [AWS CLI](https://aws.amazon.com/cli/)


Create a profile on the IAM console as shown [here](readme-images/iam-quicklinks.png). 
Next, click on "Access keys (access key ID and secret access key)," then "Create New Access Key", and then "Show Access Key."
This will create an Access Key ID and a Secret Access Key. Copy these for use in the next step.

Next, on your local workstation, create a `~/.aws/credentials` file containing the following text:

    [default]
    aws_access_key_id=ACCESS_KEY_ID
    aws_secret_access_key=SECRET_ACCESS_KEY
    region=us-east-1

`ACCESS_KEY_ID` and `SECRET_ACCESS_KEY` are the keys that you created in the previous step. 


## Driver scripts

The docker image is deployed with three additional scripts:
- `init_solver.sh`: Start sshd and runs the solver script.
- `solver`: Python script which processes intput request, runs `run_solver` and processes the output.
- `run_solver`: Bash script which runs `run_problem` (compiled) in the usual manner. If changing no_cores or schedule, this is the place.

In general, competition related scripts are placed in '/competition'
and the output of an attempt is placed in '/rundir'.


## Local testing

To test locally make sure that SATCOMP infrastructure and iProver image are built:

### Build images
SATCOMP:
```bash
cd docker && ./build_satcomp_images.sh && cd ..
```

iProver:
```bash
cd .. && docker build -t smtcomp-iprover:leader . && cd aws/
```

### Setup and run the runner

Go into the runner script, set up a docker network, give the right permissions, and attempt a test problem
```bash
cd runner
# Create test network
docker network create mallob-test
# Set the correct permissions
sudo chgrp -R 1000 . && chmod 775 .
# Run parallel test of the iProver image on a test problem
./run_parallel.sh smtcomp-iprover agree1_unsat_arrays.smt2
```

Check the output log and files in '/rundir' for any errors.
Yield `exit` to exit from the program.
The run_parallel script might have some issues with relative/absolute paths.

## AWS testing

To test on the AWS infrastructure:

```bash
cd infrastructure

# Build infrastructure image, iProver image, and push ECS storage
./quickstart-build-iprover 

# Upload test problem
./upload-problem agree1_unsat_arrays.smt2

# Run on iProver on the problem destination provided by the upload
#./quickstart-run 
./quickstart-run --s3-locations s3://401019289807-us-east-1-comp23/agree1_unsat_arrays.smt2
```

### Cleaning up
If shutting down the request of the resources, run: `./ecs-config shutdown`

If cleaning up the whole infrastructure test, run: `./delete-solver-infrastructure`


### EXTRA


For SMTCOMP-2023 we had issues running the quickstart-run script as it never stopped waiting for resources.
When going into the container on ECR on AWS, we could see under Events that we got an error message stating that 
they could not find a container with the correct resources.
If this happens again, the resource limits assigned to the profile should be checked.
