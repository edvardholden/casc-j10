#!/usr/bin/env python
import argparse
import sys
import logging
from dataclasses import dataclass
from typing import List
import time


import run.problem
from run import utils
from run.ltb import process_batch_file, LTBBatch, handle_solved_problem, LTBProblem
from run.process import is_time_left, get_time_left
from run.schedule import run_schedule, Schedule
from run.schedule import get_schedule
from run.run_parser import get_ltb_args

logging.basicConfig(
    stream=sys.stdout, level=logging.WARNING, format="%(levelname)s - %(message)s"
)
log = logging.getLogger()


@dataclass
class LTBRunSpec:
    schedule: str
    version: str
    max_timelimit: float

    def __post_init__(self):
        assert (
            self.max_timelimit > 0 or self.max_timelimit == -1
        ), "Max time args should be greater 0 or -1 (fair time)"


def check_run_specification_args(args: argparse.Namespace) -> None:
    if args.schedule is None or args.version is None or args.max_timelimit is None:
        log.error(
            "Must Specify LTB Run Specifications (schedule, version, and time limits). Exiting ..."
        )
        raise SystemExit(2)

    if not (len(args.schedule) == len(args.version) == len(args.max_timelimit)):
        log.error("LTB Run Specifications must all be of the same size. Exiting ...")
        raise SystemExit(2)

    if len(args.schedule) < 1:
        log.error("Need to provide at least one run specification")
        raise SystemExit(2)


def get_specifications_from_args(args: argparse.Namespace) -> List[LTBRunSpec]:
    # Get the data structure for the run specifications
    check_run_specification_args(args)

    specs = []
    for s, v, max_t in zip(args.schedule, args.version, args.max_timelimit):
        specs.append(LTBRunSpec(s, v, max_t))

    return specs


def terminate_batch_attempt(batch: LTBBatch) -> bool:
    if not is_time_left(batch.start_time, batch.time_limit):
        log.warning("Ran out of time, exiting...")
        return True

    if batch.get_no_unsolved_problems() < 1:
        log.info("Solved all the problems! Exiting..")
        return True

    return False


def main() -> None:
    args = get_ltb_args(sys.argv[1:])

    # Set the loglevel
    log.setLevel(args.loglevel)
    log.debug(str(args))

    # Register clean up if receiving kill signal
    utils.register_functions_and_signals()

    # Load args into run specifications
    run_specifications = get_specifications_from_args(args)
    log.info(f"Acquired {len(run_specifications)} run specifications")

    # Get the problem batch
    batch = process_batch_file(args.batch_file, args.proof_out_dir)
    log.info(f"Loaded and created LTB batch: {batch}")

    # Run each specification over the batch
    for run_spec in run_specifications:
        if terminate_batch_attempt(batch):
            log.info("Terminating..")
            break

        log.info(f"Starting attempt: {run_spec}")
        run_ltb_batch_attempt(batch, run_spec, args.no_cores, args.heuristic_context)

    log.info("Finished batch attempt")
    log.info(f"Number of unsolved problems: {batch.get_no_unsolved_problems()}")
    log.info(f"Number of solved problems: {batch.get_no_solved_problems()}")
    log.info(f"Time spent: {time.time() - batch.start_time}")

    # Clean up after the attempts
    utils.graceful_exit()


def compute_problem_attempt_time_limit(
    *, time_left: float, problems_left: int, max_timelimit: float
) -> float:
    if max_timelimit == -1:
        # Compute fair per problem time left
        per_problem_time = time_left / problems_left
        return min(per_problem_time, time_left)

    return min(max_timelimit, time_left)  # Truncate by the time left


def run_ltb_batch_attempt(
    batch: LTBBatch, spec: LTBRunSpec, no_cores: int, heuristic_context: str
) -> None:
    # Get the schedule for this configuration - always output proof, and set whether

    schedule = get_schedule(
        spec.schedule,
        heuristic_context,
        no_cores,
        **{
            "proof_out": True,  # Always on for LTB
            "problem_is_tff": run.problem.problem_version_is_tff(
                run.ltb.PROBLEM_POSTFIX_TO_PROBLEM_FORMAT[spec.version]
            ),  # Determine whether the problem is tff
        },
    )
    log.debug(str(schedule))

    no_unsolved_problems = batch.get_no_unsolved_problems()
    for i, problem in enumerate(list(batch.get_unsolved_problems())):
        schedule.reset()  # Always reset the schedule before an attempt

        # Check if we are out of time/ problems
        if terminate_batch_attempt(batch):
            return

        log.debug(f"Attempting problem number {i}: {problem}")

        time_left = get_time_left(batch.start_time, batch.time_limit)
        log.debug(f"Time left: {time_left}")

        attempt_time_limit = compute_problem_attempt_time_limit(
            time_left=time_left,
            problems_left=(no_unsolved_problems - i),
            max_timelimit=spec.max_timelimit,
        )
        log.debug("Attempt_timelimit : {:.2f}".format(attempt_time_limit))

        problem.set_path(version=spec.version)
        run_ltb_problem_attempt(batch, problem, schedule, attempt_time_limit)


def run_ltb_problem_attempt(
    batch: LTBBatch, problem: LTBProblem, schedule: Schedule, time_limit: float
) -> None:
    prob_attempt_start = time.time()
    log.debug("Time left : {get_time_left(batch.start_time, batch.time_limit):.2f}")

    # Get the problem_path from the problem
    print(f"% SZS status Started for {problem.name}")

    run_schedule(problem, schedule, time_limit)

    # Clean up files and processes
    handle_problem_post_attempt(problem, batch)
    clean_post_problem_attempt()

    log.debug(f"Problem attempt used time: {time.time() - prob_attempt_start:.2f}")


def handle_problem_post_attempt(problem: LTBProblem, batch: LTBBatch) -> None:
    if problem.solved:
        print(f"% SZS status Theorem for {problem.name}")
        handle_solved_problem(batch, problem)
    else:
        print(f"% SZS status GaveUp for {problem.name}")

    print(f"% SZS status Ended for {problem.name}")


def clean_post_problem_attempt() -> None:
    # Empty prover processes for this problem attempt
    utils.kill_all_prover_processes()

    # Clean up tmp files between each problem
    utils.clean_tmp_folder_contents()


if __name__ == "__main__":
    main()
