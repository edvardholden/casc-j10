"""
Script for loading all the heuristics in the schedules, and the schedules to verify that they work correctly.
"""

import argparse
import time
import logging
import sys
from typing import Tuple, Set

from config_schedules import SCHEDULES
from run.heuristic import get_heuristic, Heuristic
from run.problem import ProblemFormat, Problem
from run.process import ProverProcess
from run.schedule import flatten_schedule, get_schedule
from run.utils import TMP_DIR, clean_tmp_dir

PARSING_TIMEOUT = 3
# TEST_PROBLEM = "tests/res/Problems/problem_fof.p"
TEST_PROBLEM = "tests/res/Problems/agree1_unsat_arrays.smt2"
pformat = ProblemFormat.SMT2


logging.basicConfig(
    stream=sys.stdout, level=logging.WARNING, format="%(levelname)s - %(message)s"
)
log = logging.getLogger()


def get_parser(args) -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    # Set loglevel
    parser.add_argument(
        "-d",
        "--debug",
        help="Print debug logs",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Print info logs",
        action="store_const",
        dest="loglevel",
        const=logging.INFO,
    )

    return parser.parse_args(args=args)


def get_all_heuristics() -> Set[Tuple[str, int | float | None]]:
    all_heuristic_configs = []
    for sched in SCHEDULES.values():
        all_heuristic_configs += flatten_schedule(sched)

    # Remove any duplicates
    all_heuristic_configs = set(all_heuristic_configs)
    print(f"Found {len(all_heuristic_configs)} heuristics.")

    return all_heuristic_configs


def perform_parse_test(heur: Heuristic):
    # Fresh problem for each instance
    problem = Problem(TEST_PROBLEM, False, ProblemFormat.FOF)
    proc = ProverProcess(heur, problem, PARSING_TIMEOUT)
    proc.start()
    while proc.is_running():
        time.sleep(0.1)
    proc.check_errors()
    with open(proc.out_file, "r") as f:
        outs = f.read()
        if "Parsing by iProver" not in outs:
            print(
                f"Warning: parsing test on heuristic {heur} was unsuccessful outfile: {proc.out_file}"
            )


def main():
    args = get_parser(sys.argv[1:])
    # Set the loglevel
    log.setLevel(args.loglevel)
    log.debug(str(args))

    # Get hold of all heuristics:
    all_heuristic_configs = get_all_heuristics()

    # Convert all configs into heuristics - this will log warnings and errors if issues
    heuristics = [
        get_heuristic(conf_path, heuristic_context="debug", pformat=pformat)
        for conf_path, _ in all_heuristic_configs
    ]

    # Check that the process runs as expected
    for heur in heuristics:
        perform_parse_test(heur)

    # Load all schedules - any errors will be reported
    print("Number of schedules:", len(SCHEDULES))
    for sched_name in SCHEDULES:
        # Just check that they can be loaded successfully - will repeat errors from above
        get_schedule(sched_name, "debug", 1, **{"pformat": pformat})

    # Remove tmp dir if log is not debug
    if logging.root.level <= logging.DEBUG:
        print(f"In debug mode - not removing tmp out dir: {TMP_DIR}")
    else:
        print(f"Removing tmp out dir: {TMP_DIR}")
        clean_tmp_dir()


if __name__ == "__main__":
    main()  # Run tests
